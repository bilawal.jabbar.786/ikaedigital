<?php

use App\Category;
use App\Content;
use App\GeneralSettings;
use App\Headings;
use App\Prices;
use App\Services;
use App\Testimonial;
use Illuminate\Database\Seeder;

class GeneralSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        GeneralSettings::create([
            'sitename'     => 'Ikae Digital',
            'address'     => '119 W 24th Street 4th France',
            'phone'     => '+590 690 57-4700',
            'email'     => 'info@ikaedigital.com',
            'logo'     => 'front/images/logo-ikae.png',
            'footer'    => 'Tous les droits sont réservés.',
            'facebook'    => 'https://facebook.com/',
            'instagram'    => 'https://www.instagram.com/',
            'headertitle' => 'Une Vison Adapte a vos bessoins',
            'headersubtitle' => 'Digital Development',
        ]);
        Services::create([
            'h1' => 'WORDPRESS',
            'h2' => 'APPLICATION MOBILE',
            'h3' => 'APPLICATIONS WEB',
            'h4' => 'CRM & CMS',
            'h5' => 'DÉVELOPPEMENT DAPI',
            'h6' => 'CONCEPTION UI & UX',
            'h7' => 'SEO ET OPTIMISATION',
            'h8' => 'MAINTENANCE DES APPLICATIONS',
            'd1' => 'Lorem Ipsum est simplement un texte factice de lindustrie de limpression et de la composition.',
            'd2' => 'Lorem Ipsum est simplement un texte factice de lindustrie de limpression et de la composition.',
            'd3' => 'Lorem Ipsum est simplement un texte factice de lindustrie de limpression et de la composition.',
            'd4' => 'Lorem Ipsum est simplement un texte factice de lindustrie de limpression et de la composition.',
            'd5' => 'Lorem Ipsum est simplement un texte factice de lindustrie de limpression et de la composition.',
            'd6' => 'Lorem Ipsum est simplement un texte factice de lindustrie de limpression et de la composition.',
            'd7' => 'Lorem Ipsum est simplement un texte factice de lindustrie de limpression et de la composition.',
            'd8' => 'Lorem Ipsum est simplement un texte factice de lindustrie de limpression et de la composition.',
        ]);
        Prices::create([
            'title1' => 'SITE VITRINE',
            'title2' => 'SITE DYNAMIQUE PERSONNALISE',
            'title3' => 'E-COMMERCE',

            'price1' => '500',
            'price2' => '500',
            'price3' => '500',

            'd1' => '["Description 1","Description 2","Description 3","Description 4","Description 5"]',
            'd2' => '["Description 1","Description 2","Description 3","Description 4","Description 5"]',
            'd3' => '["Description 1","Description 2","Description 3","Description 4","Description 5"]',

            'longd1' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
            'longd2' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
            'longd3' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
        ]);
        Testimonial::create([
            'name' => 'Mike Andrew',
            'designation' => 'CEO  - Philandropia',
            'review' => 'Jai récemment embauché Ikaedigital pour développer une nouvelle version de mon site Web le plus populaire et je suis extrêmement satisfait du travail'
        ]);
        Testimonial::create([
            'name' => 'Mike Andrew',
            'designation' => 'CEO  - Philandropia',
            'review' => 'Jai récemment embauché Ikaedigital pour développer une nouvelle version de mon site Web le plus populaire et je suis extrêmement satisfait du travail'
        ]);
        Testimonial::create([
            'name' => 'Mike Andrew',
            'designation' => 'CEO  - Philandropia',
            'review' => 'Jai récemment embauché Ikaedigital pour développer une nouvelle version de mon site Web le plus populaire et je suis extrêmement satisfait du travail'
        ]);
        Testimonial::create([
            'name' => 'Mike Andrew',
            'designation' => 'CEO  - Philandropia',
            'review' => 'Jai récemment embauché Ikaedigital pour développer une nouvelle version de mon site Web le plus populaire et je suis extrêmement satisfait du travail'
        ]);
        Headings::create([
            'h1' => 'Nous sommes une entreprise de solutions numériques composée dinadaptés technologiques, de créatifs et dhumains curieux',
            'h2' => 'Ce que les clients disent de nous',
            'h3' => 'Nous vivons dans lespace où le design et la technologie se rencontrent. Cela nous permet damener notre créativité à de nouveaux niveaux et de livrer des.',
            'h4' => 'SENTEZ LA FORME',
            'h5' => 'Décrivez votre projet et laissez-nous vos coordonnées',
            'h6' => '',
        ]);
        Content::create([
            'video' => 'U3YuPMnjMOI',
            'about' => 'Nous sommes une équipe dindividus professionnels et énergiques avec des concepteurs talentueux et des gestionnaires expérimentés disponibles pour guider nos clients à travers lexécution sans faille et en temps opportun de tout projet de conception Web. Depuis le premier jour, nous proposons des sites Web créatifs et uniques à nos clients du monde entier.',

            'timage1' => 'front/images/calvin-fitzerald-418x315.jpg',
            'timage2' => 'front/images/luis-maxwell-418x415.jpg',
            'timage3' => 'front/images/josh-wagner-418x315.jpg',

            'tname1' => 'Amanda Smith',
            'tname2' => 'Brian King',
            'tname3' => 'George Nelson',

            'td1' => 'Director',
            'td2' => 'Marketing manager',
            'td3' => 'Designer',

            'whyimage1' => 'front/images/home-post-1-418x315.jpg',
            'whyimage2' => 'front/images/home-post-2-418x315.jpg',
            'whyimage3' => 'front/images/home-post-3-418x315.jpg',

            'whytitle1' => 'SOLUTION SUR MESURE',
            'whytitle2' => 'REPIDITE ET EFFICACITE',
            'whytitle3' => 'FACILITE DE PAIEMENT',

            'projects' => '100',
            'awards' => '45',
            'reviews' => '98',
            'customers' => '147',
        ]);

        Category::create([
            'name' => 'E-commerce',
            'photo' => 'front/images/portfolio-1-418x315.jpg'
        ]);
        Category::create([
            'name' => 'Immobilier',
            'photo' => 'front/images/portfolio-2-418x315.jpg'
        ]);
        Category::create([
            'name' => 'Location de voiture',
            'photo' => 'front/images/portfolio-3-418x315.jpg'
        ]);
        Category::create([
            'name' => 'Salon de beauté',
            'photo' => 'front/images/portfolio-4-418x315.jpg'
        ]);
        Category::create([
            'name' => 'Éducation',
            'photo' => 'front/images/portfolio-5-418x315.jpg'
        ]);
        Category::create([
            'name' => 'Auto-école',
            'photo' => 'front/images/portfolio-6-418x315.jpg'
        ]);
    }
}
