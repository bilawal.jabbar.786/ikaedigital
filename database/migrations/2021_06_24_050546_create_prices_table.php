<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices', function (Blueprint $table) {
            $table->id();
            $table->string('title1');
            $table->string('title2');
            $table->string('title3');

            $table->string('price1')->nullable();
            $table->string('price2')->nullable();
            $table->string('price3')->nullable();

            $table->longText('d1');
            $table->longText('d2');
            $table->longText('d3');

            $table->longText('longd1');
            $table->longText('longd2');
            $table->longText('longd3');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prices');
    }
}
