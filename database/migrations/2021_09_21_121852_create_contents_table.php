<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->id();
            $table->string('video');
            $table->longText('about');

            $table->string('timage1')->nullable();
            $table->string('timage2')->nullable();
            $table->string('timage3')->nullable();

            $table->string('tname1')->nullable();
            $table->string('tname2')->nullable();
            $table->string('tname3')->nullable();

            $table->string('td1')->nullable();
            $table->string('td2')->nullable();
            $table->string('td3')->nullable();

            $table->string('whyimage1')->nullable();
            $table->string('whyimage2')->nullable();
            $table->string('whyimage3')->nullable();

            $table->string('whytitle1')->nullable();
            $table->string('whytitle2')->nullable();
            $table->string('whytitle3')->nullable();

            $table->string('projects')->nullable();
            $table->string('awards')->nullable();
            $table->string('reviews')->nullable();
            $table->string('customers')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contents');
    }
}
