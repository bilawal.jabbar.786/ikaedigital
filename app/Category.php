<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function templates()
    {
        return $this->hasMany(Template::class);
    }
}
