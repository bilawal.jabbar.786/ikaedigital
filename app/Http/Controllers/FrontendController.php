<?php

namespace App\Http\Controllers;

use App\Category;
use App\Content;
use App\GeneralSettings;
use App\Headings;
use App\Leads;
use App\Payment;
use App\Portfolio;
use App\Prices;
use App\Services;
use App\Template;
use App\Testimonial;
use Illuminate\Http\Request;
use Mollie\Laravel\Facades\Mollie;

class FrontendController extends Controller
{
    public function index(){
        $services = Services::find(1);
        $prices = Prices::find(1);
        $testimonial = Testimonial::all();
        $headings = Headings::find(1);
        $content = Content::find(1);
        $category = Category::all();
        return view('front.index', compact('services', 'prices', 'testimonial', 'headings', 'content', 'category'));
    }
    public function leads(Request $request){
        $leads = new Leads();
        $leads->name = $request->name;
        $leads->email = $request->email;
        $leads->address = $request->address;
        $leads->business = $request->business;
        $leads->date = $request->date;
        $leads->budget = $request->budget;
        $leads->description = $request->description;
        $leads->phone = $request->phone;
        $leads->save();
        return response()->json(['success' => 'done'], 200);
    }
    public function pay($price, $title){
        $gs = GeneralSettings::find(1);
        return view('payment', compact('price', 'title', 'gs'));
    }
    public function payment(Request $request){
        $payment = new Payment();
        $payment->name = $request->name;
        $payment->email = $request->email;
        $payment->phone = $request->phone;
        $payment->business = $request->business;
        $payment->price = $request->price;
        $payment->title = $request->title;
        $payment->save();

        $payment = Mollie::api()->payments->create([
            "amount" => [
                "currency" => "GBP",
                "value" => number_format($request->price, 2) // You must send the correct number of decimals, thus we enforce the use of strings
            ],
            "description" => "Order #". $payment->id,
            "redirectUrl" => route('payment.success', ['id' => $payment->id]),
            "metadata" => [
                "order_id" => $payment->id,
            ],
        ]);
        return redirect($payment->getCheckoutUrl(), 303);
    }
    public function paymentsuccess($id){
        $payment = Payment::find($id);
        $payment->paymentstatus = '1';
        $payment->update();
        return view('paymentsuccess', compact('payment'));
    }
    public function service($title){
        $content = Prices::find(1);
        return view('front.service', compact('title', 'content'));
    }
    public function templates($id){
        $category = Category::find($id);
        $templates = Template::where('category_id', $id)->get();
        return view('front.templates', compact('templates', 'category'));
    }
    public function projects(){
        return view('front.projects');
    }
    public function projectsCategory($category){
        $portfolios = Portfolio::where('e2', $category)->latest()->paginate(10);
        return view('front.projects_category', compact('portfolios', 'category'));
    }
}
