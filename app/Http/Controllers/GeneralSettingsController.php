<?php

namespace App\Http\Controllers;

use App\Content;
use App\GeneralSettings;
use App\Headings;
use App\Prices;
use App\Services;
use App\Testimonial;
use Illuminate\Http\Request;

class GeneralSettingsController extends Controller
{
    public function generalsettings(){
        $gs = GeneralSettings::find(1);
        return view('generalsettings.generalsettings', compact('gs'));
    }
    public function headings(){
        $headings = Headings::find(1);
        return view('generalsettings.headings', compact('headings'));
    }
    public function services(){
        $services = Services::find(1);
        return view('generalsettings.services', compact('services'));
    }
    public function prices(){
        $prices = Prices::find(1);
        return view('generalsettings.prices', compact('prices'));
    }
    public function testimonial(){
        $testimonial = Testimonial::all();
        return view('generalsettings.testimonial.index', compact('testimonial'));
    }
    public function testimonialcreate(){
        return view('generalsettings.testimonial.create');
    }
    public function testimonialstore(Request $request){
        $testimonial = new Testimonial();
        $testimonial->name = $request->name;
        $testimonial->designation = $request->designation;
        $testimonial->review = $request->review;
        $testimonial->save();
        $notification = array(
            'messege' => 'Ajouté avec succès!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function testimonialdelete($id){
        $testimonial = Testimonial::find($id);
        $testimonial->delete();
        $notification = array(
            'messege' => 'Supprimé!',
            'alert-type' => 'error'
        );
        return redirect()->back()->with($notification);
    }
    public function generalstore(Request $request){
        $gs = GeneralSettings::find(1);
        $gs->sitename = $request->sitename;
        $gs->address = $request->address;
        $gs->phone = $request->phone;
        $gs->email = $request->email;
        $gs->footer = $request->footer;
        $gs->facebook = $request->facebook;
        $gs->instagram = $request->instagram;
        $gs->headertitle = $request->headertitle;
        $gs->headersubtitle = $request->headersubtitle;

        if ($request->hasfile('logo')) {
            $image1 = $request->file('logo');
            $name = time() . 'logo' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'logo/';
            $image1->move($destinationPath, $name);
            $gs->logo = 'logo/' . $name;
        }
        $gs->update();
        $notification = array(
            'messege' => 'Ajouté avec succès!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function servicesstore(Request $request){
        $services = Services::find(1);
        $services->h1 = $request->h1;
        $services->h2 = $request->h2;
        $services->h3 = $request->h3;
        $services->h4 = $request->h4;
        $services->h5 = $request->h5;
        $services->h6 = $request->h6;
        $services->d1 = $request->d1;
        $services->d2 = $request->d2;
        $services->d3 = $request->d3;
        $services->d4 = $request->d4;
        $services->d5 = $request->d5;
        $services->d6 = $request->d6;
        $services->update();
        $notification = array(
            'messege' => 'Ajouté avec succès!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function headingsstore(Request $request){
        $headings = Headings::find(1);
        $headings->h1 = $request->h1;
        $headings->h2 = $request->h2;
        $headings->h3 = $request->h3;
        $headings->h4 = $request->h4;
        $headings->h5 = $request->h5;
        $headings->save();
        $notification = array(
            'messege' => 'Ajouté avec succès!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function pricesstore(Request $request){
        $prices = Prices::find(1);
        $prices->title1 = $request->title1;
        $prices->title2 = $request->title2;
        $prices->title3 = $request->title3;

        $prices->price1 = $request->price1;
        $prices->price2 = $request->price2;
        $prices->price3 = $request->price3;

        $prices->longd1 = $request->longd1;
        $prices->longd2 = $request->longd2;
        $prices->longd3 = $request->longd3;

        if($request->d1){
            foreach($request->d1 as $d1)
            {
                $data[] = $d1;
                $prices->d1 = json_encode($data);
            }
        }
        if($request->d2){
            foreach($request->d2 as $d2)
            {
                $data2[] = $d2;
                $prices->d2 = json_encode($data2);
            }
        }
        if($request->d3){
            foreach($request->d3 as $d3)
            {
                $data3[] = $d3;
                $prices->d3 = json_encode($data3);
            }
        }
        $prices->update();

        $notification = array(
            'messege' => 'Ajouté avec succès!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function about(){
        $content = Content::find(1);
        return view('generalsettings.about', compact('content'));
    }
    public function aboutStore(Request $request){
        $content = Content::find(1);
        $content->video = $request->video;
        $content->about = $request->about;
        $content->projects = $request->projects;
        $content->awards = $request->awards;
        $content->reviews = $request->reviews;
        $content->customers = $request->customers;
        $content->update();
        $notification = array(
            'messege' => 'Ajouté avec succès!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function team(){
        $content = Content::find(1);
        return view('generalsettings.team', compact('content'));
    }
    public function teamStore(Request $request){
        $content = Content::find(1);

        $content->tname1 = $request->tname1;
        $content->tname2 = $request->tname2;
        $content->tname3 = $request->tname3;

        $content->td1 = $request->td1;
        $content->td2 = $request->td2;
        $content->td3 = $request->td3;

        if ($request->hasfile('timage1')) {
            $image1 = $request->file('timage1');
            $name1 = time() . 'team' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'team/';
            $image1->move($destinationPath, $name1);
            $content->timage1 = 'team/' . $name1;
        }
        if ($request->hasfile('timage2')) {
            $image2 = $request->file('timage2');
            $name2 = time() . 'team' . '.' . $image2->getClientOriginalExtension();
            $destinationPath = 'team/';
            $image2->move($destinationPath, $name2);
            $content->timage2 = 'team/' . $name2;
        }
        if ($request->hasfile('timage3')) {
            $image3 = $request->file('timage3');
            $name3 = time() . 'team' . '.' . $image3->getClientOriginalExtension();
            $destinationPath = 'team/';
            $image3->move($destinationPath, $name3);
            $content->timage3 = 'team/' . $name3;
        }

        $content->update();
        $notification = array(
            'messege' => 'Ajouté avec succès!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function why(){
        $content = Content::find(1);
        return view('generalsettings.why', compact('content'));
    }
    public function whyStore(Request $request){
        $content = Content::find(1);

        $content->whytitle1 = $request->whytitle1;
        $content->whytitle2 = $request->whytitle2;
        $content->whytitle3 = $request->whytitle3;


        if ($request->hasfile('whyimage1')) {
            $image1 = $request->file('whyimage1');
            $name1 = time() . 'why' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'why/';
            $image1->move($destinationPath, $name1);
            $content->whyimage1 = 'why/' . $name1;
        }
        if ($request->hasfile('whyimage2')) {
            $image2 = $request->file('whyimage2');
            $name2 = time() . 'why' . '.' . $image2->getClientOriginalExtension();
            $destinationPath = 'why/';
            $image2->move($destinationPath, $name2);
            $content->whyimage2 = 'why/' . $name2;
        }
        if ($request->hasfile('whyimage3')) {
            $image3 = $request->file('whyimage3');
            $name3 = time() . 'why' . '.' . $image3->getClientOriginalExtension();
            $destinationPath = 'why/';
            $image3->move($destinationPath, $name3);
            $content->whyimage3 = 'why/' . $name3;
        }

        $content->update();
        $notification = array(
            'messege' => 'Ajouté avec succès!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
}
