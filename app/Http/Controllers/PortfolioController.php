<?php

namespace App\Http\Controllers;

use App\Portfolio;
use Illuminate\Http\Request;
use Image;
class PortfolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $portfolios = Portfolio::latest()->get();
        return view('portfolio.index', compact('portfolios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'image' => 'required',
            'description' => 'required',
        ]);
        $portfolio = new Portfolio();
        $portfolio->name = $request->name;
        $portfolio->description = $request->description;
        $portfolio->e1 = $request->e1??"";
        $portfolio->e2 = $request->category??"";
        $portfolio->e3 = $request->video??"";
        $portfolio->e4 = $request->e4??"";
        $portfolio->website = $request->website;
        $portfolio->android = $request->android;
        $portfolio->ios = $request->ios;
        if ($request->image){
            $destinationPath = 'images/';
            $logo2_originalFile = $request->image->getClientOriginalName();
            $logo2_filename = strtotime("now").'-'.$logo2_originalFile;
            $request->image->move($destinationPath, $logo2_filename);
            $logo2_img = $destinationPath.$logo2_filename;
            $portfolio->image = $logo2_img;
            $path = public_path($logo2_img);
            Image::make($path)->resize(1277 , 1376)->save($path);
        }
        $portfolio->save();
        $notification = array(
            'messege' => 'Project successfully created',
            'alert-type' => 'success'
        );

        return redirect()->back()
            ->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
        ]);
        $portfolio = Portfolio::find($id);
        $portfolio->name = $request->name;
        $portfolio->description = $request->description;
        $portfolio->e1 = $request->e1??"";
        $portfolio->e2 = $request->category??"";
        $portfolio->e3 = $request->video??"";
        $portfolio->e4 = $request->e4??"";
        $portfolio->website = $request->website;
        $portfolio->android = $request->android;
        $portfolio->ios = $request->ios;
        if ($request->image){
            $destinationPath = 'images/';
            $logo2_originalFile = $request->image->getClientOriginalName();
            $logo2_filename = strtotime("now").'-'.$logo2_originalFile;
            $request->image->move($destinationPath, $logo2_filename);
            $logo2_img = $destinationPath.$logo2_filename;
            $portfolio->image = $logo2_img;
            $path = public_path($logo2_img);
            Image::make($path)->resize(1277 , 1376)->save($path);
        }
        $portfolio->save();
        $notification = array(
            'messege' => 'Project successfully created',
            'alert-type' => 'success'
        );

        return redirect()->back()
            ->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $portfolio = Portfolio::find($id);
        $portfolio->delete();
        $notification = array(
            'messege' => 'Project successfully delete',
            'alert-type' => 'success'
        );

        return redirect()->back()
            ->with($notification);
    }
}
