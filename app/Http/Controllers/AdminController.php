<?php

namespace App\Http\Controllers;

use App\Category;
use App\Leads;
use App\Payment;
use App\Template;
use Illuminate\Http\Request;
use Zip;
use ZipArchive;

class AdminController extends Controller
{
    public function leads(){
        $leads = Leads::latest()->get();
        return view('leads.index', compact('leads'));
    }
    public function payments(){
        $payments = Payment::latest()->where('paymentstatus', '=', '1')->get();
        return view('payments.index', compact('payments'));
    }
    public function categories(){
        $category = Category::all();
        return view('category.index', compact('category'));
    }
    public function categorystore(Request $request){
        $maincategory = new Category();
        $maincategory->name = $request->name;
        if ($request->hasfile('photo')) {
            $image1 = $request->file('photo');
            $name = time() . 'category' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'category/';
            $image1->move($destinationPath, $name);
            $maincategory->photo = 'category/' . $name;
        }
        $maincategory->save();
        $notification = array(
            'messege' => 'Sauvegarde réussie!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function categorydelete($id){
        $maincategory = Category::find($id);
        $maincategory->delete();
        $notification = array(
            'messege' => 'Effacer!',
            'alert-type' => 'error'
        );
        return redirect()->back()->with($notification);
    }
    public function templates(){
        $category = Category::all();
        $templates = Template::all();
        return view('category.templates', compact('templates', 'category'));
    }
    public function templateStore(Request $request){
        $template = new Template();
        $template->name = $request->name;
        $template->category_id = $request->category_id;
        if ($request->hasfile('photo')) {
            $image1 = $request->file('photo');
            $name = time() . 'category' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'category/';
            $image1->move($destinationPath, $name);
            $template->photo = 'category/' . $name;
        }
        if ($request->hasfile('template')) {
            $image2 = $request->file('template');
            $name2 = time() . 'template' . '.' . $image2->getClientOriginalExtension();
            $destinationPath = 'template/';
            $image2->move($destinationPath, $name2);
            $template->template = 'template/' . $name2;
        }
        $template->save();
        $zip = new ZipArchive;
        $res = $zip->open($template->template);
        if ($res === TRUE) {
            $zip->extractTo('demos/'.$template->id);
            $zip->close();
        }
        $template->link = 'demos/'.$template->id;
        $template->update();

        $notification = array(
            'messege' => 'Sauvegarde réussie!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function templateDelete($id){
        $template = Template::find($id);
        $template->delete();
        $notification = array(
            'messege' => 'Effacer!',
            'alert-type' => 'error'
        );
        return redirect()->back()->with($notification);
    }
    public function templateExtract($id){
        $template = Template::find($id);
        $zip = new ZipArchive;
        $res = $zip->open($template->template);
        if ($res === TRUE) {
            $zip->extractTo('myzips/');
            $zip->close();
            echo 'woot!';
        } else {
            echo 'doh!';
        }
        $notification = array(
            'messege' => 'Sauvegarde réussie!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

}
