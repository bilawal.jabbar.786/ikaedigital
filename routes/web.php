<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontendController@index')->name('front.index');
Route::get('/projects', 'FrontendController@projects')->name('front.projects');
Route::get('/0/projects/{category}', 'FrontendController@projectsCategory')->name('project.category');
Route::get('/service/details/{title}', 'FrontendController@service')->name('service.details');
Route::get('/templates/{id}', 'FrontendController@templates')->name('templates');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('portfolio', 'PortfolioController');

Route::get('/category/index', 'AdminController@categories')->name('admin.categories');
Route::post('/category/store', 'AdminController@categorystore')->name('category.store');
Route::get('/category/delete/{id}', 'AdminController@categorydelete')->name('category.delete');
Route::get('/admin/templates', 'AdminController@templates')->name('admin.templates');
Route::post('/admin/templates/store', 'AdminController@templateStore')->name('template.store');
Route::get('/templates/delete/{id}', 'AdminController@templateDelete')->name('template.delete');
Route::get('/templates/extract/{id}', 'AdminController@templateExtract')->name('template.extract');

Route::get('/admin/payments', 'AdminController@payments')->name('admin.payments');

Route::get('/general/settings', 'GeneralSettingsController@generalsettings')->name('general.settings');
Route::post('/general/settings/store', 'GeneralSettingsController@generalstore')->name('general.store');

Route::get('/admin/leads', 'AdminController@leads')->name('admin.leads');

Route::get('/general/headings', 'GeneralSettingsController@headings')->name('general.headings');
Route::post('/general/headings/store', 'GeneralSettingsController@headingsstore')->name('headings.store');

Route::get('/general/services', 'GeneralSettingsController@services')->name('general.services');
Route::post('/general/services', 'GeneralSettingsController@servicesstore')->name('service.store');

Route::get('/general/about', 'GeneralSettingsController@about')->name('general.about');
Route::post('/about/store', 'GeneralSettingsController@aboutStore')->name('about.store');

Route::get('/general/team', 'GeneralSettingsController@team')->name('general.team');
Route::post('/team/store', 'GeneralSettingsController@teamStore')->name('team.store');

Route::get('/general/why', 'GeneralSettingsController@why')->name('general.why');
Route::post('/why/store', 'GeneralSettingsController@whyStore')->name('why.store');

Route::get('/general/prices', 'GeneralSettingsController@prices')->name('general.prices');
Route::post('/general/prices/store', 'GeneralSettingsController@pricesstore')->name('price.store');

Route::get('/general/testimonial', 'GeneralSettingsController@testimonial')->name('general.testimonial');
Route::get('/testimonial/create', 'GeneralSettingsController@testimonialcreate')->name('testimonial.create');
Route::get('/testimonial/delete/{id}', 'GeneralSettingsController@testimonialdelete')->name('testimonial.delete');
Route::post('/testimonial/store', 'GeneralSettingsController@testimonialstore')->name('testimonial.store');

Route::post('/front/leads', 'FrontendController@leads')->name('front.leads');

Route::get('/front/pay/{price}/{title}', 'FrontendController@pay')->name('front.pay');
Route::post('/front/payment', 'FrontendController@payment')->name('front.payment');

Route::get('/payment/success/{id}', 'FrontendController@paymentsuccess')->name('payment.success');
