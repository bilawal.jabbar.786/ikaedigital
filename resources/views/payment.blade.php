<!DOCTYPE html>
<html lang="zxx" class="js">
<head>
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="{{asset('front/images/favicon.png')}}">
    <!-- Site Title  -->
    <title>{{$gs->sitename}}</title>
    <!-- Bundle and Base CSS -->
    <link rel="stylesheet" href="{{asset('front/assets/css/vendor.bundle.css?ver=141')}}">
    <link rel="stylesheet" href="{{asset('front/assets/css/style.css?ver=141')}}">
    <link rel="stylesheet" href="{{asset('front/assets/css/theme.css?ver=141')}}">


</head>

<body class="body-wider bg-dark tc-bunker">
<!-- Header -->
<header class="is-transparent header-s2 is-sticky is-shrink" id="header">
    <div class="header-main">
        <div class="header-container container">
            <div class="header-wrap">
                <!-- Logo  -->
                <div class="header-logo logo">
                    <a href="./" class="logo-link">
                        <img class="logo-light" src="{{asset($gs->logo)}}" style="height: 100px" srcset="{{asset($gs->logo)}}" alt="logo">
                    </a>
                </div>

                <!-- Menu Toogle -->
                <div class="header-nav-toggle">
                    <a href="#" class="search search-mobile search-trigger"><i class="icon ti-search "></i></a>
                    <a href="#" class="navbar-toggle" data-menu-toggle="header-menu">
                        <div class="toggle-line">
                            <span></span>
                        </div>
                    </a>
                </div>
                <!-- Menu Toogle -->

                <!-- Menu -->
                <div class="header-navbar">
                    <nav class="header-menu" id="header-menu">
                        <ul class="menu">
                            <li class="menu-item">
                                <a class="menu-link nav-link active" href="{{route('front.index')}}">Home</a>
                            </li>
                        </ul>
                    </nav>
                </div><!-- .header-navbar -->

                <!-- header-search -->
                <div class="header-search">
                    <form role="search" method="POST" class="search-form" action="#">
                        <div class="search-group">
                            <input type="text" class="input-search" placeholder="Search ...">
                            <button class="search-submit" type="submit"><i class="icon ti-search"></i></button>
                        </div>
                    </form>
                </div>
                <!-- . header-search -->
            </div>
        </div>
    </div>
</header>
<!-- end header -->

<!-- section contact -->
<div class="section section-x tc-bunker" >
    <div class="container" id="contact">
        <div class="row">
            <div class="col-xl-4 col-lg-8">
                <div class="section-head">
                    <h5 class="heading-xs dash">Payez maintenant</h5>
                    <h2>Prix: {{$price}} €</h2>
                </div>
            </div><!-- .col -->
        </div><!-- .row -->
        <div class="row gutter-vr-30px">
            <!-- .col -->
            <div class="col-lg-8">
                <form class="form-dark mt-10" action="{{route('front.payment')}}" method="POST">
                    @csrf
                    <div class="form-results" style="display: none">Merci d'avoir soumis vos informations. Notre équipe vous contactera bientôt.</div>
                    <div class="row">
                        <div class="form-field col-md-6">
                            <input name="name" type="text" placeholder="Votre nom" class="input bdr-b required">
                        </div>
                        <div class="form-field col-md-6">
                            <input name="email" type="email" placeholder="Votre e-mail" class="input bdr-b required">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-field col-md-6">
                            <input name="phone" type="text" placeholder="Votre Telephone" class="input bdr-b required">
                        </div>
                        <div class="form-field col-md-6">
                            <input name="business" type="text" placeholder="Votre entreprise" class="input bdr-b required">
                        </div>
                    </div>
                    <input type="hidden" value="{{$price}}" name="price">
                    <input type="hidden" value="{{$title}}" name="title">
                    <div class="row">
                        <div class="form-field col-md-12">
                            <textarea name="description" placeholder="Parlez-nous brièvement de votre projet. " class="input input-msg bdr-b required"></textarea>
                            <input type="text" class="d-none" name="form-anti-honeypot" value="">
                            <button type="submit" class="btn">Continuez</button>
                        </div>
                    </div>
                </form>
            </div><!-- .col -->
        </div>
    </div><!-- .container -->
</div>
<!-- .section -->
<!-- footer -->
<footer class="section footer tc-bunker footer-s3">
    <div class="container">
        <div class="row gutter-vr-30px justify-content-sm-between justify-content-center">
            <div class="col-lg-3 col-md-4 text-center text-md-left">
                <div class="wgs">
                    <div class="wgs-content">
                        <div class="wgs-logo">
                            <a href="#">
                                <img src="{{asset($gs->logo)}}" srcset="{{asset($gs->logo)}}" alt="logo">
                            </a>
                        </div>
                        <p>&copy; 2021. {{$gs->footer}}</p>
                    </div>
                </div><!-- .wgs -->
            </div><!-- .col -->
            <div class="col-lg-6 col-md-4 text-center">
                <div class="wgs mtm-5">
                    <div class="wgs-content">
                        <h4 class="mb-10">Connectons-nous</h4>
                        <ul class="social social-style-icon social-s3">
                            <li><a href="{{$gs->facebook}}" class="fab fa-facebook"></a></li>
                            <li><a href="{{$gs->instagram}}" class="fab fa-instagram"></a></li>
                        </ul>
                    </div>
                </div><!-- .wgs -->
            </div><!-- .col -->
            <div class="col-lg-2 col-md-4 offset-lg-1 text-center text-md-right">
                <div class="wgs mtm-8">
                    <div class="wgs-content tc-light">
                        <ul class="wgs-menu">
                            <li><a href="#">About us</a></li>
                            <li><a href="#">Why Ikae Digital?</a></li>
                            <li><a href="#">Meet the team</a></li>
                        </ul>
                    </div>
                </div><!-- .wgs -->
            </div><!-- .col -->
        </div><!-- .row -->
    </div><!-- .container -->
</footer>
<!-- .footer -->

<!-- preloader -->
<div class="preloader preloader-dark preloader-jackson no-split"><span class="spinner spinner-alt"><img class="spinner-brand" src="images/logo-white.png" alt=""></span></div>

<!-- JavaScript -->
<script src="{{asset('front/assets/js/jquery.bundle.js?ver=141')}}"></script>
<script src="{{asset('front/assets/js/scripts.js?ver=141')}}"></script>
</body>
</html>
