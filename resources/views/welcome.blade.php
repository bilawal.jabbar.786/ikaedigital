<!DOCTYPE html>
<html lang="zxx" class="js">
<head>
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="{{asset('front/images/favicon.png')}}">
    <!-- Site Title  -->
    <title>{{$gs->sitename}}</title>
    <!-- Bundle and Base CSS -->
    <link rel="stylesheet" href="{{asset('front/assets/css/vendor.bundle.css?ver=141')}}">
    <link rel="stylesheet" href="{{asset('front/assets/css/style.css?ver=141')}}">
    <link rel="stylesheet" href="{{asset('front/assets/css/theme.css?ver=141')}}">


</head>

<body class="body-wider bg-dark tc-bunker">
<!-- Header -->
<header class="is-transparent header-s2 is-sticky is-shrink" id="header">
    <div class="header-main">
        <div class="header-container container">
            <div class="header-wrap">
                <!-- Logo  -->
                <div class="header-logo logo">
                    <a href="./" class="logo-link">
                        <img class="logo-light" src="{{asset($gs->logo)}}" style="height: 100px" srcset="{{asset($gs->logo)}}" alt="logo">
                    </a>
                </div>

                <!-- Menu Toogle -->
                <div class="header-nav-toggle">
                    <a href="#" class="search search-mobile search-trigger"><i class="icon ti-search "></i></a>
                    <a href="#" class="navbar-toggle" data-menu-toggle="header-menu">
                        <div class="toggle-line">
                            <span></span>
                        </div>
                    </a>
                </div>
                <!-- Menu Toogle -->

                <!-- Menu -->
                <div class="header-navbar">
                    <nav class="header-menu" id="header-menu">
                        <ul class="menu">
                            <li class="menu-item">
                                <a class="menu-link nav-link active" href="{{route('front.index')}}">Home</a>
                            </li>
                            <li class="menu-item">
                                <a class="menu-link nav-link menu-toggle" href="#services">Services</a>
                            </li>
                            <li class="menu-item"><a class="menu-link nav-link" href="#testimonial">Témoignage</a></li>
                            <li class="menu-item"><a class="menu-link nav-link" href="#pricing">Tarif</a></li>
                            <li class="menu-item">
                                <a class="menu-link nav-link menu-toggle" href="#contact">Contacter</a>
                            </li>
                        </ul>
                        <ul class="menu-btns">
                            <li><a href="" class="btn search search-trigger"><i class="icon ti-search "></i></a></li>
                            <li><a href="#contact" class="btn btn-sm">Démarrer un projet</a></li>
                        </ul>
                    </nav>
                </div><!-- .header-navbar -->

                <!-- header-search -->
                <div class="header-search">
                    <form role="search" method="POST" class="search-form" action="#">
                        <div class="search-group">
                            <input type="text" class="input-search" placeholder="Search ...">
                            <button class="search-submit" type="submit"><i class="icon ti-search"></i></button>
                        </div>
                    </form>
                </div>
                <!-- . header-search -->
            </div>
        </div>
    </div>
    <div class="banner banner-s3 tc-light" id="home">
        <div class="banner-block">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-xl-8 text-center">
                        <div class="banner-content">
                            <h1 class="banner-heading size-sm">{{$gs->headertitle}} </h1>
                            <p class="lead">{{$gs->headersubtitle}}</p>
                            <ul class="social social-style-icon">
                                <li><a href="{{$gs->facebook}}" class="fab fa-facebook-f"></a></li>
                                <li><a href="{{$gs->instagram}}" class="fab fa-instagram"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bg-image overlay-gradient">
                <img src="{{asset('front/images/img.jpeg')}}" alt="banner">
            </div>
        </div>
        <div class="banner-btn text-center">
            <div class="scroll scroll-s2">
                <h6 class="t-u">Défiler vers le bas</h6>
                <a href="#about" class="menu-link">
                    <span class="scroll-icon"></span>
                </a>
            </div>
        </div>
    </div>
    <!-- .banner -->
</header>
<!-- end header -->

<!-- section / about -->
<div class="section section-xx tc-bunker" id="about">
    <div class="container">
        <div class="row gutter-vr-30px justify-content-around">
            <div class="col-lg-12 col-xl-12 text-center text-lg-left">
                <div class="text-box mrm-10">
                    <h2 class="fz-1 mb-0">{{$headings->h1}}</h2>
                </div>
            </div><!-- .col -->
        </div><!-- .row -->
    </div><!-- .container -->
</div>
<!-- .section -->

<!-- section / feature -->
<div class="section tc-bunker section-xx pt-0" id="services">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 text-center">
                <div class="section-head section-md">
                    <h2 class="fz-1 mb-0">Nos services</h2>
                </div>
            </div>
        </div>
        <div class="row gutter-vr-40px">
            <div class="col-xl-3 col-sm-6 text-center">
                <div class="feature feature-alt feature-alt-s2">
                    <div class="feature-icon">
                        <em class="icon ti-panel"></em>
                    </div>
                    <div class="feature-content">
                        <h4>{{$services->h1}}</h4>
                        <p>{{$services->d1}}</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6 text-center">
                <div class="feature feature-alt feature-alt-s2">
                    <div class="feature-icon">
                        <em class="icon ti-write"></em>
                    </div>
                    <div class="feature-content">
                        <h4>{{$services->h2}}</h4>
                        <p>{{$services->d2}}</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6 text-center">
                <div class="feature feature-alt feature-alt-s2">
                    <div class="feature-icon">
                        <em class="icon ti-layers-alt"></em>
                    </div>
                    <div class="feature-content">
                        <h4>{{$services->h3}} </h4>
                        <p>{{$services->d3}}</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6 text-center">
                <div class="feature feature-alt feature-alt-s2">
                    <div class="feature-icon">
                        <em class="icon ti-panel"></em>
                    </div>
                    <div class="feature-content">
                        <h4>{{$services->h4}}</h4>
                        <p>{{$services->d4}}</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6 text-center">

            </div>
            <div class="col-xl-3 col-sm-6 text-center">
                <div class="feature feature-alt feature-alt-s2">
                    <div class="feature-icon">
                        <em class="icon ti-write"></em>
                    </div>
                    <div class="feature-content">
                        <h4>{{$services->h5}}</h4>
                        <p>{{$services->d5}}</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6 text-center">
                <div class="feature feature-alt feature-alt-s2">
                    <div class="feature-icon">
                        <em class="icon ti-layers-alt"></em>
                    </div>
                    <div class="feature-content">
                        <h4>{{$services->h6}}</h4>
                        <p>{{$services->d6}}</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6 text-center">

            </div>
        </div><!-- .row -->
    </div>
    <div class="bg-image overlay-gradient-full">
        <img src="{{asset('front/images/bg-f.png')}}" alt="">
    </div>
</div>
<!-- .section -->

<!-- section prices -->
<div class="section section-x tc-bunker pt-0" id="pricing" >
    <div class="container container-lg-custom" style="max-width: 100% !important;">
        <div class="row justify-content-center">
            <div class="col-md-8 text-center">
                <div class="section-head section-md">
                    <h2 class="fz-1 mb-0">Forfaits tarifaires</h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center gutter-vr-30px text-center">
            <div class="col-lg-3 col-sm-12">
                <div class="pricing-boxed pricing-boxed-sm-bg bg-bunker">
                    <div class="pricing-price">
                        <h3>{{$prices->price1}}<span class="price-unit price-unit-lg"> €</span><span class="price-for">{{$prices->title1}}</span></h3>
                    </div>
                    <div class="pricing-feature">
                        <ul>
                            @foreach(json_decode($prices->d1) as $item)
                                <li>{{$item}}</li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="pricing-cta pricing-cta-lg">
                        <a href="{{route('front.pay', ['price' => $prices->price1, 'title' => $prices->title1])}}" class="btn btn-lg-s2 btn-capitalize">Acheter</a>
                    </div>
                </div>
            </div><!-- .col -->

            <div class="col-lg-3 col-sm-12">
                <div class="pricing-boxed pricing-boxed-sm-bg bg-bunker">
                    <div class="pricing-price">
                        <h3>{{$prices->price2}}<span class="price-unit price-unit-lg">€</span><span class="price-for">{{$prices->title2}}</span></h3>
                    </div>
                    <div class="pricing-feature">
                        <ul>
                            @foreach(json_decode($prices->d2) as $item)
                                <li>{{$item}}</li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="pricing-cta pricing-cta-lg">
                        <a href="{{route('front.pay', ['price' => $prices->price2, 'title' => $prices->title2])}}" class="btn btn-lg-s2 btn-capitalize">Acheter</a>
                    </div>
                </div>
            </div><!-- .col -->

            <div class="col-lg-3 col-sm-12">
                <div class="pricing-boxed pricing-boxed-sm-bg bg-bunker">
                    <div class="pricing-price">
                        <h3>{{$prices->price3}}<span class="price-unit price-unit-lg">€</span><span class="price-for">{{$prices->title3}}</span></h3>
                    </div>
                    <div class="pricing-feature">
                        <ul>
                            @foreach(json_decode($prices->d3) as $item)
                                <li>{{$item}}</li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="pricing-cta pricing-cta-lg">
                        <a href="{{route('front.pay', ['price' => $prices->price3, 'title' => $prices->title3])}}" class="btn btn-lg-s2 btn-capitalize">Acheter</a>
                    </div>
                </div>
            </div><!-- .col -->

            <div class="col-lg-3 col-sm-12">
                <div class="pricing-boxed pricing-boxed-sm-bg bg-bunker">
                    <div class="pricing-price">
                        <h3>{{$prices->price4}}<span class="price-unit price-unit-lg">€</span><span class="price-for">{{$prices->title4}}</span></h3>
                    </div>
                    <div class="pricing-feature">
                        <ul>
                            @foreach(json_decode($prices->d4) as $item)
                                <li>{{$item}}</li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="pricing-cta pricing-cta-lg">
                        <a href="{{route('front.pay', ['price' => $prices->price4, 'title' => $prices->title4])}}" class="btn btn-lg-s2 btn-capitalize">Acheter</a>
                    </div>
                </div>
            </div><!-- .col -->

        </div><!-- .row -->
    </div>

</div>
<!-- .section -->

<!-- section / testimonial -->
<div class="section section-xx" id="testimonial">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-lg-3">
                <div class="section-head res-m-btm mrm-4">
                    <h5 class="heading-xs tc-primary">Témoignage</h5>
                    <h2 class="fz-1">{{$headings->h2}}</h2>
                </div>
            </div><!-- .col -->
            <div class="col-lg-6 offset-lg-1 col-md-8">
                <div class="section-head">
                    <p class="lead">{{$headings->h3}}</p>
                </div>
            </div><!-- .col -->
        </div><!-- .row -->

        <div class="tes tes-s3 tes-s3-alt mt-5">
            <div class="has-carousel" data-items="3" data-loop="true" data-auto="true" data-dots="true">
                @foreach($testimonial as $testi)
                <div class="tes-block">
                    <div class="tes-content tes-content-s2 tes-bg">
                        <h5 class="heading-xs dash">TÉMOIGNAGE</h5>
                        <p>
                        {{$testi->review}}
                        </p>
                    </div>
                    <div class="tes-author d-flex align-items-center">
                        <div class="author-con author-con-s2">
                            <h6 class="author-name t-u">{{$testi->name}}</h6>
                            <p class="tc-light">{{$testi->designation}}</p>
                        </div>
                    </div>
                </div>
                @endforeach

            </div><!-- .row -->
        </div>
    </div><!-- .container -->
    <!-- bg -->
    <div class="bg-image overlay-gradient-full">
        <img src="{{asset('front/images/bg-f.png')}}" alt="">
    </div>
    <!-- .bg -->
</div id>
<!-- .section -->


<!-- section contact -->
<div class="section section-x tc-bunker" >
    <div class="container" id="contact">
        <div class="row">
            <div class="col-xl-4 col-lg-8">
                <div class="section-head">
                    <h5 class="heading-xs dash">{{$headings->h4}}</h5>
                    <h2>{{$headings->h5}}</h2>
                </div>
            </div><!-- .col -->
        </div><!-- .row -->
        <div class="row gutter-vr-30px">
            <div class="col-lg-4 order-lg-last">
                <div class="contact-text contact-text-s2 bg-secondary box-pad ">
                    <div class="text-box">
                        <h4>{{$gs->sitename}}</h4>
                        <p class="lead">{{$gs->address}}</p>
                    </div>
                    <ul class="contact-list">
                        <li>
                            <em class="contact-icon ti-mobile"></em>
                            <div class="conatct-content">
                                <a href="tel:19173303116">{{$gs->phone}}</a>
                            </div>
                        </li>
                        <li>
                            <em class="contact-icon ti-email"></em>
                            <div class="conatct-content">
                                <a href="mailto:hello@this.work.com">{{$gs->email}}</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- .col -->
            <div class="col-lg-8">
                <form class="genox-form form-dark mt-10" action="{{route('front.leads')}}" method="POST">
                    @csrf
                    <div class="form-results" style="display: none">Merci d'avoir soumis vos informations. Notre équipe vous contactera bientôt.</div>
                    <div class="row">
                        <div class="form-field col-md-6">
                            <input name="name" type="text" placeholder="Votre nom" class="input bdr-b required">
                        </div>
                        <div class="form-field col-md-6">
                            <input name="email" type="email" placeholder="Votre e-mail" class="input bdr-b required">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-field col-md-6">
                            <input name="address" type="text" placeholder="Votre adresse" class="input bdr-b required">
                        </div>
                        <div class="form-field col-md-6">
                            <input name="business" type="text" placeholder="Votre entreprise" class="input bdr-b required">
                        </div>
                        <div class="form-field col-md-6">
                            <input name="phone" type="text" placeholder="Votre Telephone" class="input bdr-b required">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-field col-md-6">
                            <input type="text" name="date" class="input bdr-b datepicker" required placeholder="Quand voulez-vous commencer ?">
                        </div>
                        <div class="form-field form-select col-md-6">
                            <select name="budget" class="form-control input-select bdr-b input required" id="selectid_b">
                                <option>Quel est votre budget</option>
                                <option value="200  € - 500 €">200  € - 500 €</option>
                                <option value="500  € - 1000 €">500  € - 1000 €</option>
                                <option value="1000 € - 1500 €">1000 € - 1500 €</option>
                                <option value="1500 € - 2000 €">1500 € - 2000 €</option>
                                <option value="2000 € - 2500 €">2000 € - 2500 €</option>
                                <option value="3000 € - 3500 €">3000 € - 3500 €</option>
                                <option value="3500 € - 4000 €">3500 € - 4000 €</option>
                                <option value="4000 € - 4500 €">4000 € - 4500 €</option>
                                <option value="4500 € - 5000 €">4500 € - 5000 €</option>
                                <option value="5000 € - 5500 €">5000 € - 5500 €</option>
                                <option value="5500 € - 6000 €">5500 € - 6000 €</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-field col-md-12">
                            <textarea name="description" placeholder="Parlez-nous brièvement de votre projet" class="input input-msg bdr-b required"></textarea>
                            <input type="text" class="d-none" name="form-anti-honeypot" value="">
                            <button type="submit" class="btn">Envoyer le message</button>
                        </div>
                    </div>
                </form>
            </div><!-- .col -->
        </div>
    </div><!-- .container -->
</div>
<!-- .section -->
<!-- footer -->
<footer class="section footer tc-bunker footer-s3">
    <div class="container">
        <div class="row gutter-vr-30px justify-content-sm-between justify-content-center">
            <div class="col-lg-3 col-md-4 text-center text-md-left">
                <div class="wgs">
                    <div class="wgs-content">
                        <div class="wgs-logo">
                            <a href="#">
                                <img src="{{asset($gs->logo)}}" srcset="{{asset($gs->logo)}}" alt="logo">
                            </a>
                        </div>
                        <p>&copy; 2021. {{$gs->footer}}</p>
                    </div>
                </div><!-- .wgs -->
            </div><!-- .col -->
            <div class="col-lg-6 col-md-4 text-center">
                <div class="wgs mtm-5">
                    <div class="wgs-content">
                        <h4 class="mb-10">Connectons-nous</h4>
                        <ul class="social social-style-icon social-s3">
                            <li><a href="{{$gs->facebook}}" class="fab fa-facebook"></a></li>
                            <li><a href="{{$gs->instagram}}" class="fab fa-instagram"></a></li>
                        </ul>
                    </div>
                </div><!-- .wgs -->
            </div><!-- .col -->
            <div class="col-lg-2 col-md-4 offset-lg-1 text-center text-md-right">
                <div class="wgs mtm-8">
                    <div class="wgs-content tc-light">
                        <ul class="wgs-menu">
                            <li><a href="#">About us</a></li>
                            <li><a href="#">Why Ikae Digital?</a></li>
                            <li><a href="#">Meet the team</a></li>
                        </ul>
                    </div>
                </div><!-- .wgs -->
            </div><!-- .col -->
        </div><!-- .row -->
    </div><!-- .container -->
</footer>
<!-- .footer -->

<!-- preloader -->
<div class="preloader preloader-dark preloader-jackson no-split"><span class="spinner spinner-alt"><img class="spinner-brand" src="images/logo-white.png" alt=""></span></div>

<!-- JavaScript -->
<script src="{{asset('front/assets/js/jquery.bundle.js?ver=141')}}"></script>
<script src="{{asset('front/assets/js/scripts.js?ver=141')}}"></script>
</body>
</html>
