<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('home')}}" class="brand-link">
      <span class="brand-text font-weight-light pl-4"><strong>Admin</strong></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <img src="{{asset('assets/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{Auth::user()->name}}</a>
        </div>
      </div>
    @php

        if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
        {
          $link = "https";
        }
        else
        {
          $link = "http";

          // Here append the common URL characters.
          $link .= "://";

          // Append the host(domain name, ip) to the URL.
          $link .= $_SERVER['HTTP_HOST'];

          // Append the requested resource location to the URL
          $link .= $_SERVER['REQUEST_URI'];
        }

    @endphp
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item menu-open">
            <a href="{{route('home')}}" class="nav-link {{ $link == route('home') ? 'active':'' }}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                  Tableau de bord
              </p>
            </a>
          </li>
            <li class="nav-item">
                <a href="{{route('admin.leads')}}" class="nav-link">
                    <i class="nav-icon fas fa-ellipsis-h"></i>
                    <p>
                        Pistes
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('general.settings')}}" class="nav-link">
                    <i class="nav-icon fas fa-ellipsis-h"></i>
                    <p>
                        Réglages Généraux
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('general.about')}}" class="nav-link">
                    <i class="nav-icon fas fa-ellipsis-h"></i>
                    <p>
                        À propos de nous
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('general.team')}}" class="nav-link">
                    <i class="nav-icon fas fa-ellipsis-h"></i>
                    <p>
                        NOTRE EQUIPE
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('general.why')}}" class="nav-link">
                    <i class="nav-icon fas fa-ellipsis-h"></i>
                    <p>
                        POURQUOI NOUS
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('portfolio.index')}}" class="nav-link">
                    <i class="nav-icon fas fa-ellipsis-h"></i>
                    <p>
                        Projets
                    </p>
                </a>
            </li>
<!--            <li class="nav-item">
                <a href="{{route('general.services')}}" class="nav-link">
                    <i class="nav-icon fas fa-ellipsis-h"></i>
                    <p>
                        Services
                    </p>
                </a>
            </li>-->
            <li class="nav-item">
                <a href="{{route('general.prices')}}" class="nav-link">
                    <i class="nav-icon fas fa-ellipsis-h"></i>
                    <p>

                        NOS FORMULES

                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('general.testimonial')}}" class="nav-link">
                    <i class="nav-icon fas fa-ellipsis-h"></i>
                    <p>
                        Témoignage
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-ellipsis-h"></i>
                    <p>
                        Modèles
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{route('admin.categories')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Catégories </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('admin.templates')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Modèles</p>
                        </a>
                    </li>
                </ul>
            </li>
<!--
            <li class="nav-item">
                <a href="{{route('admin.payments')}}" class="nav-link">
                    <i class="nav-icon fas fa-ellipsis-h"></i>
                    <p>
                        Paiements
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('general.headings')}}" class="nav-link">
                    <i class="nav-icon fas fa-ellipsis-h"></i>
                    <p>
                        En-têtes
                    </p>
                </a>
            </li>
-->


           <li class="nav-item">
            <a href="{{route('logout')}}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
                                         class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                Log Out
              </p>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
            </form>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
