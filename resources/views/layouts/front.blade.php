<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
<head>
    <?php
    $gs = \App\GeneralSettings::find(1);
    ?>
    <!-- Site Title-->
    <title>{{$gs->sitename}}</title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <link rel="icon" href="{{asset($gs->logo)}}" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css2?family=Fira+Sans:ital,wght@0,300;0,600;0,800;1,800&amp;family=Open+Sans:ital,wght@0,300;0,400;1,400&amp;display=swap">
    <link rel="stylesheet" href="{{asset('front/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/fonts.css')}}">
    <style>.ie-panel{display: none;background: #212121;padding: 10px 0;box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3);clear: both;text-align:center;position: relative;z-index: 1;} html.ie-10 .ie-panel, html.lt-ie-10 .ie-panel {display: block;}
        .rd-navbar .rd-navbar-nav > li > a {
            font-size: 14px;
            line-height: 1.28;
            font-weight: 900;
            text-transform: uppercase;
            letter-spacing: .05em;
        }
        .rd-navbar-static {
            display: block;
            padding: 0 15px;
            background-color: black;
            box-shadow: 0 2px 12px rgba(136, 136, 136, 0.1);
        }
        .rd-navbar-static .rd-navbar-top-panel {
            border-bottom: 0px solid #ececee;
        }
        .rd-navbar .rd-navbar-nav > li > a {
            font-size: 14px;
            line-height: 1.28;
            font-weight: 900;
            text-transform: uppercase;
            letter-spacing: .05em;
            color: white;
        }
        @media only screen and (max-width: 600px) {
            .rd-navbar .rd-navbar-nav > li > a {
                font-size: 14px;
                line-height: 1.28;
                font-weight: 900;
                text-transform: uppercase;
                letter-spacing: .05em;
                color: black;
            }
        }
    </style>
</head>
<body>
<div class="ie-panel"><a href="http://windows.microsoft.com/en-US/internet-explorer/">
        <img src="{{asset('front/images/ie8-panel/warning_bar_0000_us.jpg')}}" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
{{--<div class="preloader">--}}
{{--    <div class="cssload-container">--}}
{{--        <svg class="filter">--}}
{{--            <defs>--}}
{{--                <filter id="gooeyness">--}}
{{--                    <fegaussianblur in="SourceGraphic" stddeviation="10" result="blur"></fegaussianblur>--}}
{{--                    <fecolormatrix in="blur" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 20 -10" result="gooeyness"></fecolormatrix>--}}
{{--                    <fecomposite in="SourceGraphic" in2="gooeyness" operator="atop"></fecomposite>--}}
{{--                </filter>--}}
{{--            </defs>--}}
{{--        </svg>--}}
{{--        <div class="dots">--}}
{{--            <div class="dot"></div>--}}
{{--            <div class="dot"></div>--}}
{{--            <div class="dot"></div>--}}
{{--            <div class="dot"></div>--}}
{{--            <div class="dot"></div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
<!-- Page-->
<div class="page">
    <header class="page-header">
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap">
            <nav class="rd-navbar rd-navbar-default" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-fixed" data-xl-layout="rd-navbar-static" data-xxl-layout="rd-navbar-static" data-device-layout="rd-navbar-fixed" data-sm-device-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-fixed" data-xl-device-layout="rd-navbar-static" data-xxl-device-layout="rd-navbar-static" data-xl-stick-up-offset="50px" data-xxl-stick-up-offset="50px">
                <!-- RD Navbar Top Panel-->
                <div class="rd-navbar-top-panel rd-navbar-top-panel-dark">
                    <div class="rd-navbar-top-panel__main">
                        <div class="rd-navbar-top-panel__toggle rd-navbar-fixed__element-1 rd-navbar-static--hidden" data-rd-navbar-toggle=".rd-navbar-top-panel__main"><span></span></div>
                        <div class="rd-navbar-top-panel__content">
                            <div class="rd-navbar-top-panel__left">
                                <ul class="rd-navbar-items-list">
                                    <li>
                                        <div class="unit flex-row align-items-center unit-spacing-xs">
                                            <div class="unit__left"><span class="icon icon-sm icon-primary linear-icon-map-marker"></span></div>
                                            <div class="unit__body">
                                                <p><a href="#">Adresse: {{$gs->address}}</a></p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="unit flex-row align-items-center unit-spacing-xs">
                                            <div class="unit__left"><span class="icon icon-sm icon-primary linear-icon-telephone"></span></div>
                                            <div class="unit__body">
                                                <ul class="list-semicolon">
                                                    <li><a href="tel:{{$gs->phone}}">{{$gs->phone}}</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="rd-navbar-top-panel__right">
                                <ul class="list-inline-xxs">
                                    <li><a class="icon icon-xxs icon-gray-darker fa fa-facebook" href="{{$gs->facebook}}"></a></li>
                                    <li><a class="icon icon-xxs icon-gray-darker fa fa-twitter" href="#"></a></li>
                                    <li><a class="icon icon-xxs icon-gray-darker fa fa-instagram" href="{{$gs->instagram}}"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="rd-navbar-inner rd-navbar-search-wrap">
                    <!-- RD Navbar Panel-->
                    <div class="rd-navbar-panel rd-navbar-search-lg_collapsable">
                        <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
                        <!-- RD Navbar Brand-->
                        <div class="rd-navbar-brand">
                            <!--Brand--><a class="brand" href="{{route('front.index')}}">
                                <img class="brand-logo-dark" src="{{asset($gs->logo)}}" alt="" width="108" height="40" loading="lazy"/>
                                <img class="brand-logo-light" src="{{asset($gs->logo)}}" alt="" width="108" height="40" loading="lazy"/></a>
                        </div>
                    </div>
                    <!-- RD Navbar Nav-->
                    <div class="rd-navbar-nav-wrap rd-navbar-search_not-collapsable">
                        <!-- RD Navbar Nav-->
                        <div class="rd-navbar__element rd-navbar-search_collapsable">
                            <button class="rd-navbar-search__toggle rd-navbar-fixed--hidden" data-rd-navbar-toggle=".rd-navbar-search-wrap"></button>
                        </div>
                        <!-- RD Search-->
                        <div class="rd-navbar-search rd-navbar-search_toggled rd-navbar-search_not-collapsable">
                            <form class="rd-search" action="#" method="GET" data-search-live="rd-search-results-live">
                                <div class="form-wrap">
                                    <input class="form-input" id="rd-navbar-search-form-input" type="text" name="s" autocomplete="off">
                                    <label class="form-label" for="rd-navbar-search-form-input">Entrez un mot-clef</label>
                                    <div class="rd-search-results-live" id="rd-search-results-live"></div>
                                </div>
                                <button class="rd-search__submit" type="submit"></button>
                            </form>
                            <div class="rd-navbar-fixed--hidden">
                                <button class="rd-navbar-search__toggle" data-custom-toggle=".rd-navbar-search-wrap" data-custom-toggle-disable-on-blur="true"></button>
                            </div>
                        </div>
                        <div class="rd-navbar-search_collapsable">
                            <ul class="rd-navbar-nav">
                                <li class="rd-nav-item active"><a class="rd-nav-link" href="{{route('front.index')}}#services">NOS SERVICES</a>
                                </li>
                                <li class="rd-nav-item"><a href="{{route('front.index')}}#about" class="rd-nav-link">QUI SOMMES NOUS</a>
                                </li>
                                <!--                                <li class="rd-nav-item"><a class="rd-nav-link" href="#">VIDEO PROMO 2D</a>
                                                                </li>-->
                                <li class="rd-nav-item"><a class="rd-nav-link" href="{{route('front.index')}}#websites">VOTRE SITE WEB CLÉ EN MAIN</a>
                                </li>
                                <li class="rd-nav-item"><a href="{{route('front.index')}}#formulas" class="rd-nav-link">NOS FORMULES</a>
                                </li>
                                <li class="rd-nav-item"><a href="{{route('front.index')}}#testimony" class="rd-nav-link">TÉMOIGNAGES</a>
                                </li>
                                <li class="rd-nav-item"><a href="{{route('front.projects')}}" class="rd-nav-link">NOS RÉALISATIONS</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </header>

    @yield('content')

<!-- Page Footer-->
    <section class="pre-footer-corporate bg-image-7 bg-overlay-darkest">
        <div class="container">
            <div class="row justify-content-sm-center justify-content-lg-start row-30 row-md-60">
                <div class="col-sm-10 col-md-6 col-lg-10 col-xl-3">
                    <h6>À propos de nous</h6>
                    <p>{{$gs->footer}}</p>
                </div>
                <div class="col-sm-10 col-md-6 col-lg-3 col-xl-3">
                    <h6>Navigation</h6>
                    <ul class="list-xxs">
                        <li><a href="#services">NOS SERVICES</a></li>
                        <li><a href="#about">QUI SOMMES NOUS</a></li>
                        <li><a href="#">VIDEO PROMO 2D</a></li>
                        <li><a href="#websites">VOTRE SITE WEB CLÉ EN MAIN</a></li>
                        <li><a href="#formulas">NOS FORMULES</a></li>
                    </ul>
                </div>
                <div class="col-sm-10 col-md-6 col-lg-5 col-xl-3">
                    <h6>Nos services</h6>
                    <ul class="list-xxs list-primary">
                        <li>
                            <a href="#">DÉVELOPPEMENT D'API</a>
                        </li>
                        <li>
                            <a href="#">WORDPRESS</a>
                        </li>
                        <li>
                            <a href="#">APPLICATION MOBILE</a>
                        </li>
                        <li>
                            <a href="#">APPLICATIONS WEB</a>
                        </li>
                        <li>
                            <a href="#">CRM & CMS</a>
                        </li>
                        <li>
                            <a href="#">DÉVELOPPEMENT D'API</a>
                        </li>
                        <li>
                            <a href="#">CONCEPTION UI & UX</a>
                        </li>
                        <li>
                            <a href="#">SEO ET OPTIMISATION</a>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-10 col-md-6 col-lg-4 col-xl-3">
                    <h6>Contacts</h6>
                    <ul class="list-xs">
                        <li>
                            <dl class="list-terms-minimal">
                                <dt>Adresse</dt>
                                <dd>{{$gs->address}}</dd>
                            </dl>
                        </li>
                        <li>
                            <dl class="list-terms-minimal">
                                <dt>Téléphone</dt>
                                <dd>
                                    <ul class="list-semicolon">
                                        <li><a href="tel:{{$gs->phone}}">{{$gs->phone}}</a></li>
                                    </ul>
                                </dd>
                            </dl>
                        </li>
                        <li>
                            <dl class="list-terms-minimal">
                                <dt>E-mail</dt>
                                <dd><a class="link-primary" href="mailto:#">{{$gs->email}}</a></dd>
                            </dl>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <footer class="footer-corporate bg-gray-darkest">
        <div class="container">
            <div class="footer-corporate__inner">
                <p class="rights"><span>IKAE DIGITAL<span>&nbsp;&copy;&nbsp;</span><span class="copyright-year"></span>. Tous les droits sont réservés.<span>&nbsp;</span>
                        <a href="#">Conditions d'utilisation et politique de confidentialité</a></span></p>
                <ul class="list-inline-xxs">
                    <li><a class="icon icon-xxs icon-primary fa fa-facebook" href="#"></a></li>
                    <li><a class="icon icon-xxs icon-primary fa fa-twitter" href="#"></a></li>
                    <li><a class="icon icon-xxs icon-primary fa fa-instagram" href="#"></a></li>
                </ul>
            </div>
        </div>
    </footer>
</div>
<!-- Global Mailform Output-->
<div class="snackbars" id="form-output-global"></div>
<!-- Javascript-->
<script src="{{asset('front/js/core.min.js')}}"></script>
<script src="{{asset('front/js/script.js')}}"></script>
</body>
</html>
