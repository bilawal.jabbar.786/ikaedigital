@extends('layouts.admin')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Domicile</a></li>
                            <li class="breadcrumb-item active">
                                NOS FORMULES
                            </li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- SELECT2 EXAMPLE -->
                <div class="card card-default">
                    <div class="card-header">
                        <h3 class="card-title">
                            NOS FORMULES
                        </h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <form action="{{route('price.store')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                    @csrf
                    <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Titre 1</label>
                                        <input type="text" name="title1" value="{{$prices->title1}}" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="">La description</label>
                                        <div id="d1">
                                            @foreach(json_decode($prices->d1) as $item)
                                                <input type="text" name="d1[]" value="{{$item}}" placeholder="Entrez la description" class="form-control" required>
                                            @endforeach
                                        </div>

                                        <br>
                                        <div class="text-center">
                                        <a onclick="addd1()" class="btn btn-sm btn-success">Ajouter</a>
                                        <a onclick="removed1()" class="btn btn-sm btn-danger">Supprimer</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Titre 2</label>
                                            <input type="text" name="title2" value="{{$prices->title2}}" class="form-control" required>
                                        </div>

                                    <div class="form-group">
                                        <label for="">La description</label>
                                        <div id="d2">
                                            @foreach(json_decode($prices->d2) as $item)
                                                <input type="text" name="d2[]" value="{{$item}}" placeholder="Entrez la description" class="form-control" required>
                                            @endforeach
                                        </div>

                                        <br>
                                        <div class="text-center">
                                            <a onclick="addd2()" class="btn btn-sm btn-success">Ajouter</a>
                                            <a onclick="removed2()" class="btn btn-sm btn-danger">Supprimer</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Titre 3</label>
                                            <input type="text" name="title3" value="{{$prices->title3}}" class="form-control" required>
                                        </div>


                                    <div class="form-group">
                                        <label for="">La description</label>
                                        <div id="d3">
                                            @foreach(json_decode($prices->d3) as $item)
                                                <input type="text" name="d3[]" value="{{$item}}" placeholder="Entrez la description" class="form-control" required>
                                            @endforeach
                                        </div>

                                        <br>
                                        <div class="text-center">
                                            <a onclick="addd3()" class="btn btn-sm btn-success">Ajouter</a>
                                            <a onclick="removed3()" class="btn btn-sm btn-danger">Supprimer</a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Description détaillée de la Formule 1</label>
                                        <textarea name="longd1" class="form-control" id="summernote" cols="30" rows="10">{{$prices->longd1}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Description détaillée de la Formule 2</label>
                                        <textarea name="longd2" class="form-control" id="summernote1" cols="30" rows="10">{{$prices->longd2}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Description détaillée de la Formule 3</label>
                                        <textarea name="longd3" class="form-control" id="summernote2" cols="30" rows="10">{{$prices->longd3}}</textarea>
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary">Sauvegarder</button>
                        </div>
                    </form>
                    <!-- /.card -->

                    <!-- /.container-fluid -->
        </section>
    </div>
@endsection
@section('script')
    <script>
        function addd1(){
            $('#d1').append('<input type="text" name="d1[]" placeholder="Entrez la description" class="form-control" required>');
        }
        function removed1(){
            $('#d1 input').last().remove();
        }
        function addd2(){
            $('#d2').append('<input type="text" name="d2[]" placeholder="Entrez la description" class="form-control" required>');
        }
        function removed2(){
            $('#d2 input').last().remove();
        }
        function addd3(){
            $('#d3').append('<input type="text" name="d3[]" placeholder="Entrez la description" class="form-control" required>');
        }
        function removed3(){
            $('#d3 input').last().remove();
        }
        function addd4(){
            $('#d4').append('<input type="text" name="d4[]" placeholder="Entrez la description" class="form-control" required>');
        }
        function removed4(){
            $('#d4 input').last().remove();
        }
    </script>
@endsection
