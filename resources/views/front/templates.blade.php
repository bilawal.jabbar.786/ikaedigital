@extends('layouts.front')
@section('content')
    <section class="section-lg text-center" id="niche">
        <div class="bg-decor d-flex align-items-center justify-content-end" data-parallax-scroll="{&quot;y&quot;: 150,  &quot;smoothness&quot;: 30}"><img src="images/bg-decor-4.png" alt="" loading="lazy"/>
        </div>
        <div class="container container-spacing-30">
            <h4>Tous les modèles de démo de {{$category->name}}</h4>
            <div class="row row-60 offset-top-2 justify-content-center">
                @foreach($templates as $template)
                <div class="col-lg-4 col-md-6">
                    <a target="_blank" class="thumbnail-type-2" href="{{url($template->link)}}">
                        <figure>
                            <img src="{{asset($template->photo)}}" width="350" height="350" alt="">
                        </figure>
                        <div class="caption">
                            <div class="caption-title">{{$template->name}}</div>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
