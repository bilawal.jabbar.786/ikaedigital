@extends('layouts.front')
@section('content')
    <section class="section-md bg-gray-lighter text-center decor-text" data-content="RÉALISATIONS">
        <div class="bg-decor d-flex align-items-start" data-parallax-scroll="{&quot;y&quot;: -50,  &quot;smoothness&quot;: 30}">
            <img src="{{asset('front/images/bg-decor-7.png')}}" alt="" loading="lazy"/>
        </div>
        <div class="container offset-top-1">
            <h4 class="heading-decorated">NOS RÉALISATIONS</h4>
            <div class="row row-50">
                <div class="col-md-6 col-lg-4">
                    <!-- Thumb corporate-->
                    <div class="thumb thumb-corporate">
                        <div class="thumb-corporate__main">
                            <img style="border-radius: 20px" src="https://img.freepik.com/free-vector/web-development-programmer-engineering-coding-website-augmented-reality-interface-screens-developer-project-engineer-programming-software-application-design-cartoon-illustration_107791-3863.jpg" alt="" width="418" height="315" loading="lazy"/>
                        </div>
                        <div class="thumb-corporate__caption">
                            <p class="thumb__title"><a href="{{route('project.category', ['category' => 'SITE WEB'])}}">SITE WEB</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <!-- Thumb corporate-->
                    <div class="thumb thumb-corporate">
                        <div class="thumb-corporate__main">
                            <img style="border-radius: 20px" src="https://thumbs.dreamstime.com/b/mobile-application-development-creative-design-teamwork-concept-coworkers-creating-smartphone-app-interface-flat-vector-129541875.jpg" alt="" width="418" height="415" loading="lazy"/>
                        </div>
                        <div class="thumb-corporate__caption">
                            <p class="thumb__title"><a href="{{route('project.category', ['category' => 'APPLICATION MOBILE'])}}">APPLICATION MOBILE</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <!-- Thumb corporate-->
                    <div class="thumb thumb-corporate">
                        <div class="thumb-corporate__main">
                            <img style="border-radius: 20px" src="https://media.istockphoto.com/id/1265041897/vector/business-team-working-together-on-web-page-design-people-building-website-interface-on.jpg?s=612x612&w=0&k=20&c=0nwzJe_VQNlN94Own93LE5pqnYG5g8E1ez7M4u0NWvk=" alt="" width="418" height="315" loading="lazy"/>
                        </div>
                        <div class="thumb-corporate__caption">
                            <p class="thumb__title"><a href="{{route('project.category', ['category' => 'BUILDER'])}}">BUILDER</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <!-- Thumb corporate-->
                    <div class="thumb thumb-corporate">
                        <div class="thumb-corporate__main">
                            <img style="border-radius: 20px" src="https://img.freepik.com/free-vector/hand-drawn-flat-design-crm-illustration_23-2149379498.jpg" alt="" width="418" height="315" loading="lazy"/>
                        </div>
                        <div class="thumb-corporate__caption">
                            <p class="thumb__title"><a href="{{route('project.category', ['category' => 'CRM'])}}">CRM</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <!-- Thumb corporate-->
                    <div class="thumb thumb-corporate">
                        <div class="thumb-corporate__main">
                            <img style="border-radius: 20px" src="https://img.freepik.com/free-vector/man-with-easel-before-screen-with-video-learning-how-draw-portrait-illustration_335657-267.jpg?w=2000" alt="" width="418" height="315" loading="lazy"/>
                        </div>
                        <div class="thumb-corporate__caption">
                            <p class="thumb__title"><a href="{{route('project.category', ['category' => 'VIDÉO 2D'])}}">VIDÉO 2D</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <!-- Thumb corporate-->
                    <div class="thumb thumb-corporate">
                        <div class="thumb-corporate__main">
                            <img style="border-radius: 20px" src="https://img.freepik.com/free-vector/gamers-using-different-devices-playing-mobile-phone-tablet-laptop-console-cartoon-illustration_74855-14380.jpg" alt="" width="418" height="315" loading="lazy"/>
                        </div>
                        <div class="thumb-corporate__caption">
                            <p class="thumb__title"><a href="{{route('project.category', ['category' => 'JEUX'])}}">JEUX</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
