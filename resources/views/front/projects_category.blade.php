@extends('layouts.front')
@section('content')
    <section class="object-wrap decor-text" data-content="About">
        <div class="bg-decor d-flex align-items-center justify-content-center"
             data-parallax-scroll="{&quot;y&quot;: 70, &quot;x&quot;: 50, &quot;smoothness&quot;: 30}"
             style="transform:translate3d(23.813px, 33.327px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(0deg) scaleX(1) scaleY(1) scaleZ(1); -webkit-transform:translate3d(23.813px, 33.327px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(0deg) scaleX(1) scaleY(1) scaleZ(1); ">
            <img src="images/bg-decor-10.png" alt="" loading="lazy">
        </div>
        <div style="padding: 60px 0;" class="section-lg">
            <h4 class="heading-decorated text-center mb-4">{{$category}}</h4>
            <div>
                @foreach($portfolios as $key => $portfolio)
                    @if($key%2 == 0 )
                        <section class="section-lg bg-gray-lighter" id="formulas">
                            <div class="bg-decor"
                                 data-parallax-scroll="{&quot;x&quot;: 80, &quot;y&quot;: -80,  &quot;smoothness&quot;: 30}">
                                <img src="{{asset('front/images/bg-decor-3.png')}}" alt="" loading="lazy"/>
                            </div>
                            <div class="bg-decor d-flex align-items-center"
                                 data-parallax-scroll="{&quot;x&quot;: 80,  &quot;smoothness&quot;: 30}">
                                <img src="{{asset('front/images/bg-decor-1.png')}}" alt="" loading="lazy"/>
                            </div>
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h4 class="heading-decorated">{{$portfolio->name}}</h4>
                                        <p>{!! $portfolio->description !!}</p>
                                        <div class="row">
                                            @if($portfolio->website)
                                                <div class="col-xl-4 mb-4">
                                                    <a target="_blank" href="{{$portfolio->website}}"
                                                       class="button button-primary button-shadow"
                                                       style="opacity: 1; transform: translateY(0px) translateX(0px);">Visitez
                                                        le site Web</a>
                                                </div>
                                            @endif
                                            @if($portfolio->android)
                                                <div class="col-xl-4 mb-4">
                                                    <a target="_blank" href="{{$portfolio->android}}"
                                                       class="button button-primary button-shadow"
                                                       style="background-color: #f16d99; border-color: #f16d99;  opacity: 1; transform: translateY(0px) translateX(0px);">App
                                                        Android</a>
                                                </div>
                                            @endif
                                            @if($portfolio->ios)
                                                <div class="col-xl-4 mb-4">
                                                    <a target="_blank" href="{{$portfolio->ios}}"
                                                       class="button button-primary button-shadow"
                                                       style="background-color: #f16d99; border-color: #f16d99;  opacity: 1; transform: translateY(0px) translateX(0px);">App
                                                        Ios</a>
                                                </div>
                                            @endif
                                            @if($portfolio->e1)
                                                <div class="col-xl-4 mb-4">
                                                    <a target="_blank" href="{{$portfolio->e1}}"
                                                       class="button button-primary button-shadow"
                                                       style="background-color: #f16d99; border-color: #f16d99;  opacity: 1; transform: translateY(0px) translateX(0px);">FlipBook</a>
                                                </div>
                                            @endif
                                            @if($portfolio->e3)
                                                <div class="col-xl-4 mb-4">
                                                    <a target="_blank" href="{{$portfolio->e3}}"
                                                       class="button button-primary button-shadow"
                                                       style="background-color: #f16d99; border-color: #f16d99;  opacity: 1; transform: translateY(0px) translateX(0px);">VIDÉO</a>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class=""><img src="{{asset($portfolio->image)}}" alt=""></div>
                                    </div>
                                </div>
                            </div>
                        </section>

                    @else
                        <section class="section-lg bg-gray-lighter" id="formulas">
                            <div class="bg-decor"
                                 data-parallax-scroll="{&quot;x&quot;: 80, &quot;y&quot;: -80,  &quot;smoothness&quot;: 30}">
                                <img src="{{asset('front/images/bg-decor-3.png')}}" alt="" loading="lazy"/>
                            </div>
                            <div class="bg-decor d-flex align-items-center"
                                 data-parallax-scroll="{&quot;x&quot;: 80,  &quot;smoothness&quot;: 30}">
                                <img src="{{asset('front/images/bg-decor-1.png')}}" alt="" loading="lazy"/>
                            </div>
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class=""><img src="{{asset($portfolio->image)}}" alt=""></div>
                                    </div>
                                    <div class="col-lg-6">
                                        <h4 class="heading-decorated">{{$portfolio->name}}</h4>
                                        <p>{!! $portfolio->description !!}</p>
                                        <div class="row">
                                            @if($portfolio->website)
                                                <div class="col-xl-4 mb-4">
                                                    <a target="_blank" href="{{$portfolio->website}}"
                                                       class="button button-primary button-shadow"
                                                       style="opacity: 1; transform: translateY(0px) translateX(0px);">Visitez
                                                        le site Web</a>
                                                </div>
                                            @endif
                                            @if($portfolio->android)
                                                <div class="col-xl-4 mb-4">
                                                    <a target="_blank" href="{{$portfolio->android}}"
                                                       class="button button-primary button-shadow"
                                                       style="background-color: #f16d99; border-color: #f16d99;  opacity: 1; transform: translateY(0px) translateX(0px);">App
                                                        Android</a>
                                                </div>
                                            @endif
                                            @if($portfolio->ios)
                                                <div class="col-xl-4 mb-4">
                                                    <a target="_blank" href="{{$portfolio->ios}}"
                                                       class="button button-primary button-shadow"
                                                       style="background-color: #f16d99; border-color: #f16d99;  opacity: 1; transform: translateY(0px) translateX(0px);">App
                                                        Ios</a>
                                                </div>
                                            @endif
                                            @if($portfolio->e1)
                                                <div class="col-xl-4 mb-4">
                                                    <a target="_blank" href="{{$portfolio->e1}}"
                                                       class="button button-primary button-shadow"
                                                       style="background-color: #f16d99; border-color: #f16d99;  opacity: 1; transform: translateY(0px) translateX(0px);">FlipBook</a>
                                                </div>
                                            @endif
                                            @if($portfolio->e3)
                                                <div class="col-xl-4 mb-4">
                                                    <a target="_blank" href="{{$portfolio->e3}}"
                                                       class="button button-primary button-shadow"
                                                       style="background-color: #f16d99; border-color: #f16d99;  opacity: 1; transform: translateY(0px) translateX(0px);">VIDÉO</a>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    @endif
                @endforeach
            </div>
            <br>
            <br>
            <div class="d-flex align-items-center justify-content-center">{{$portfolios->links()}}</div>
        </div>
    </section>
@endsection
