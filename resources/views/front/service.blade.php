@extends('layouts.front')
@section('content')
    <section>
        <div class="container" style="padding: 50px">
            @if($title == $content->title1)
            <h4>{{$title}}</h4>
            <p>{!! $content->longd1 !!}</p>
            @endif
            @if($title == $content->title2)
            <h4>{{$title}}</h4>
                    <p>{!! $content->longd2 !!}</p>
            @endif
            @if($title == $content->title3)
            <h4>{{$title}}</h4>
                    <p>{!! $content->longd3 !!}</p>
            @endif
        </div>
    </section>

@endsection
