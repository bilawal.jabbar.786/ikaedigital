@extends('layouts.front')
@section('content')
    <!-- Swiper-->
    <svg class="defs">
        <defs>
            <lineargradient id="gradient1" x1="0%" y1="0%" x2="100%" y2="100%">
                <stop offset="50%" stop-color="#AA54ED">
                    <animate attributename="stop-color" values="#AA54ED; #5348E2; #AA54ED" dur="5s" repeatcount="indefinite"></animate>
                </stop>
                <stop offset="100%" stop-color="#5348E2">
                    <animate attributename="stop-color" values="#5348E2; #AA54ED; #5348E2" dur="5s" repeatcount="indefinite"></animate>
                </stop>
            </lineargradient>
        </defs>
    </svg>

    <div class="swiper-container swiper-slider swiper-slider_fullheight swiper-digital" data-effect="circle-bg" data-loop="false" data-autoplay="4600" data-speed="1600" data-mousewheel="false" data-keyboard="false" data-circle-fill="url(#gradient1)">
        <div class="swiper-wrapper">
            <div class="swiper-slide" data-circle-cx=".3" data-circle-cy=".5" data-circle-r=".14" style="background-image: url({{asset('front/06.png')}});">
                <div class="swiper-slide-caption" style="width: 100%">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-xxl-5 d-flex justify-content-center">
<!--                                <div>
                                    <h6 class="text-white text-end" data-swiper-anime="{ &quot;animation&quot;: &quot;swiperContentRide&quot;, &quot;duration&quot;: 800, &quot;delay&quot;: 500 }">WEB Agency</h6>
                                    <h1 class="text-white" data-swiper-anime="{ &quot;animation&quot;: &quot;swiperContentRide&quot;, &quot;duration&quot;: 800, &quot;delay&quot;: 700 }"><span class="text-lowercase heading-4 text-white">the</span><span>FUTURE</span></h1>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide" data-circle-cx=".7" data-circle-cy=".5" data-circle-r=".2" style="background-image: url({{asset('front/images/slider-2.png')}});">
                <div class="swiper-slide-caption text-center" style="width: 100%">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <h2 class="text-white" data-swiper-anime="{ &quot;animation&quot;: &quot;swiperContentRide&quot;, &quot;duration&quot;: 800, &quot;delay&quot;: 500 }">Construit par des geeks et utilisé par les humains</h2>
                                <h5 class="text-white text-width-2 block-centered" data-swiper-anime="{ &quot;animation&quot;: &quot;swiperContentRide&quot;, &quot;duration&quot;: 800, &quot;delay&quot;: 700 }">IKAE DIGITAL vise à satisfaire des besoins réels de projets réels. Nous avons un pack d'outils pour cela.</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide" data-circle-cx=".6" data-circle-cy=".5" data-circle-r=".18" style="background-image: url({{asset('front/images/slider-3.png')}});">
                <div class="swiper-slide-caption d-flex justify-content-center">
                    <div>
                        <h4 class="text-white text-end" data-swiper-anime="{ &quot;animation&quot;: &quot;swiperContentRide&quot;, &quot;duration&quot;: 800, &quot;delay&quot;: 200 }">IKAE DIGITAL</h4>
                        <h1 class="text-white bg-accent p-2 pt-lg-3 ps-lg-3 pe-lg-3" data-swiper-anime="{ &quot;animation&quot;: &quot;swiperContentRide&quot;, &quot;duration&quot;: 800, &quot;delay&quot;: 400 }"><span class="text-lowercase heading-4 text-white">le</span><span>FUTUR</span></h1>
                        <h4 class="text-white text-start mt-1" data-swiper-anime="{ &quot;animation&quot;: &quot;swiperContentRide&quot;, &quot;duration&quot;: 800, &quot;delay&quot;: 600 }">AVENIR DES SERVICES INFORMATIQUES NUMÉRIQUES</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-pagination"></div>
        <div class="swiper-button-prev linear-icon-chevron-left"></div>
        <div class="swiper-button-next linear-icon-chevron-right"></div>
    </div>
    <!-- Our Services-->
    <section class="services section-xl bg-default text-center">
        <div class="bg-decor d-flex align-items-center" data-parallax-scroll="{&quot;y&quot;: 100,  &quot;smoothness&quot;: 30}">
            <img src="{{asset('front/images/bg-decor-1.png')}}" alt="" loading="lazy"/>
        </div>
        <div class="bg-decor d-flex align-items-end" data-parallax-scroll="{&quot;x&quot;: -150, &quot;from-scroll&quot;: 420, &quot;smoothness&quot;: 30}">
            <img src="{{asset('front/images/bg-decor-2.png')}}" alt="" loading="lazy"/>
        </div>
        <div class="bg-decor" data-parallax-scroll="{&quot;x&quot;: 80, &quot;y&quot;: 80,  &quot;smoothness&quot;: 30}">
            <img src="{{asset('front/images/bg-decor-3.png')}}" alt="" loading="lazy"/>
        </div>
        <div class="container" id="services">
            <h4 class="heading-decorated">NOS SERVICES</h4>
            <!-- Circle carousel-->
            <div class="carousel-wrapper">
                <div class="circle-carousel" data-speed="1000" data-autoplay="5000">
                    <!-- slides-->
                    <div class="slides">
                        <div class="content-box">
                            <div class="content-box-inner">
                                <div class="content-box-img bg-overlay-dark" style="background-image: url({{asset('front/images/service-1.jpg')}})">
                                    <div class="content-title-wrap">
                                        <div class="content-title">
                                            <h4><a href="#">WORDPRESS</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-box">
                            <div class="content-box-inner">
                                <div class="content-box-img" style="background-image: url({{asset('front/images/service-2.jpg')}})">
                                    <div class="content-title-wrap">
                                        <div class="content-title">
                                            <h4><a href="#">APPLICATION MOBILE</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-box">
                            <div class="content-box-inner">
                                <div class="content-box-img" style="background-image: url({{asset('front/images/service-3.jpg')}})">
                                    <div class="content-title-wrap">
                                        <div class="content-title">
                                            <h4><a href="#">APPLICATIONS WEB</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-box">
                            <div class="content-box-inner">
                                <div class="content-box-img bg-overlay-dark" style="background-image: url({{asset('front/images/service-4.jpg')}})">
                                    <div class="content-title-wrap">
                                        <div class="content-title">
                                            <h4><a href="#">CRM & CMS</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-box">
                            <div class="content-box-inner">
                                <div class="content-box-img" style="background-image: url({{asset('front/images/service-5.jpg')}})">
                                    <div class="content-title-wrap">
                                        <div class="content-title">
                                            <h4><a href="#">DÉVELOPPEMENT D'API</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-box">
                            <div class="content-box-inner">
                                <div class="content-box-img bg-overlay-dark" style="background-image: url({{asset('front/images/service-6.jpg')}})">
                                    <div class="content-title-wrap">
                                        <div class="content-title">
                                            <h4><a href="#">CONCEPTION UI & UX</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-box">
                            <div class="content-box-inner">
                                <div class="content-box-img" style="background-image: url({{asset('front/images/service-7.jpg')}})">
                                    <div class="content-title-wrap">
                                        <div class="content-title">
                                            <h4><a href="#">SEO ET OPTIMISATION</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-box">
                            <div class="content-box-inner">
                                <div class="content-box-img" style="background-image: url({{asset('front/images/service-8.jpg')}})">
                                    <div class="content-title-wrap">
                                        <div class="content-title">
                                            <h4><a href="#">MAINTENANCE DES APPLICATIONS</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- pagination-->
                    <div class="pagination">
                        <div class="item">
                            <div class="dot"><i class="linear-icon-pencil-ruler"></i><span></span></div>
                        </div>
                        <div class="item">
                            <div class="dot"><i class="linear-icon-users"></i><span></span></div>
                        </div>
                        <div class="item">
                            <div class="dot"><i class="linear-icon-wall"></i><span></span></div>
                        </div>
                        <div class="item">
                            <div class="dot"><i class="linear-icon-apartment"></i><span></span></div>
                        </div>
                        <div class="item">
                            <div class="dot"><i class="linear-icon-home4"></i><span></span></div>
                        </div>
                        <div class="item">
                            <div class="dot"><i class="linear-icon-pencil-ruler2"></i><span></span></div>
                        </div>
                        <div class="item">
                            <div class="dot"><i class="linear-icon-magic-wand"></i><span></span></div>
                        </div>
                        <div class="item">
                            <div class="dot"><i class="linear-icon-menu3"></i><span></span></div>
                        </div>
                    </div>
                    <div class="prev"><span>PRÉC</span></div>
                    <div class="next"><span>SUIVANT</span></div>
                </div>
            </div>
        </div>
    </section>
    <!-- About us-->
    <section class="bg-gray-lighter object-wrap decor-text" data-content="About" id="about">
        <div class="bg-decor d-flex align-items-center justify-content-end" data-parallax-scroll="{&quot;y&quot;: 50,  &quot;smoothness&quot;: 30}">
            <img src="{{asset('front/images/bg-decor-4.png')}}" alt="" loading="lazy"/>
        </div>
        <div class="section-lg">
            <div class="container">
                <div class="row justify-content-end">
                    <div class="col-lg-6 position-relative">
                        <h4 class="heading-decorated">QUI SOMMES NOUS?</h4>
                        <p>{{$content->about}}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="object-wrap__body object-wrap__body-sizing-1 object-wrap__body-md-left bg-image" style="background-image: url({{asset('front/01.png')}})"></div>
    </section>
    <section class="section-md bg-default decor-text" data-content="Features">
        <div class="bg-decor d-flex align-items-center" data-parallax-scroll="{&quot;x&quot;: -50,  &quot;smoothness&quot;: 30}">
            <img src="{{asset('front/images/bg-decor-5.png')}}" alt="" loading="lazy"/>
        </div>
        <div class="container">
            <div class="row justify-content-md-center justify-content-lg-between row-50 align-items-center">
                <div class="col-md-8 col-lg-6 position-relative">
                    <h4 class="heading-decorated">VIDEO PROMO 2D</h4>
                    <!-- Blurb minimal-->
                    <iframe id="video" width="100%" height="400" src="https://www.youtube.com/embed/{{$content->video}}">
                    </iframe>
                </div>
                <div class="col-md-7 col-lg-4">
                    <figure class="image-sizing-1" data-parallax-scroll="{&quot;y&quot;: -50,  &quot;smoothness&quot;: 30}">
                        <img src="{{asset('front/05.png')}}" alt="" width="1481" height="2068" loading="lazy"/>
                    </figure>
                </div>
            </div>
        </div>
    </section>
    <!-- Call to Action-->

    <!-- portfolio-->
    <section class="section-md bg-default text-center">
        <div class="bg-decor d-flex align-items-center" data-parallax-scroll="{&quot;y&quot;: 50,  &quot;smoothness&quot;: 30}">
            <img src="{{asset('front/images/bg-decor-6.png')}}" alt="" loading="lazy"/>
        </div>
        <div class="container" id="websites">
            <h4 class="heading-decorated">VOTRE SITE WEB CLE EN MAIN</h4>
            <div class="isotope-wrap row row-70">
                <div class="col-sm-12">
                    <div class="isotope row" data-isotope-layout="fitRows" data-isotope-group="gallery">
                        @foreach($category as $cat)
                        <div class="col-12 col-md-6 col-lg-4 isotope-item" data-filter="Category 2">
                            <a class="img-thumbnail-variant-3" href="{{route('templates', ['id' => $cat->id])}}">
                                <img src="{{asset($cat->photo)}}" alt="" width="418" height="315" loading="lazy"/>
                                <div class="caption"><span class="icon hover-top-element linear-icon-folder-picture"></span>
                                    <ul class="list-inline-tag hover-top-element">
                                        <li>{{$cat->templates->count()}} Démos</li>
                                    </ul>
                                    <p class="heading-5 hover-top-element">{{$cat->name}}</p>
                                    <div class="divider"></div>
                                </div>
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>

        <section class="section section-sm context-dark bg-gray-darker section-cta">
        <div class="container">
            <div class="row row-50 align-items-center justify-content-center justify-content-xl-between">
              <div class="col-md-4">
                  <h2 style="text-align: center; color: #2cbfb5">-LOGICIEL-</h2>
              </div>
                <div class="col-md-4">
                    <h2 style="text-align: center">CRM / CMS</h2>
              </div>
                <div class="col-md-4">
                    <h2 style="text-align: center; color: #2cbfb5">-SOLUTION-</h2>
              </div>
            </div>
        </div>
    </section>

    <!-- Meet Our Team-->
    <section class="section-md bg-gray-lighter text-center decor-text" data-content="Team">
        <div class="bg-decor d-flex align-items-start" data-parallax-scroll="{&quot;y&quot;: -50,  &quot;smoothness&quot;: 30}">
                <img src="{{asset('front/images/bg-decor-7.png')}}" alt="" loading="lazy"/>
        </div>
        <div class="container">
            <h4 class="heading-decorated">NOTRE EQUIPE</h4>
            <div class="row row-50 offset-top-1">
                <div class="col-md-6 col-lg-4">
                    <!-- Thumb corporate-->
                    <div class="thumb thumb-corporate">
                        <div class="thumb-corporate__main">
                            <img src="{{asset($content->timage1)}}" alt="" width="418" height="315" loading="lazy"/>
                        </div>
                        <div class="thumb-corporate__caption">
                            <p class="thumb__title"><a href="#">{{$content->tname1}}</a></p>
                            <p class="thumb__subtitle">{{$content->td1}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <!-- Thumb corporate-->
                    <div class="thumb thumb-corporate">
                        <div class="thumb-corporate__main">
                            <img src="{{asset($content->timage2)}}" alt="" width="418" height="415" loading="lazy"/>
                        </div>
                        <div class="thumb-corporate__caption">
                            <p class="thumb__title"><a href="#">{{$content->tname2}}</a></p>
                            <p class="thumb__subtitle">{{$content->td2}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <!-- Thumb corporate-->
                    <div class="thumb thumb-corporate">
                        <div class="thumb-corporate__main">
                            <img src="{{asset($content->timage3)}}" alt="" width="418" height="315" loading="lazy"/>
                        </div>
                        <div class="thumb-corporate__caption">
                            <p class="thumb__title"><a href="#">{{$content->tname3}}</a></p>
                            <p class="thumb__subtitle">{{$content->td3}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Posts-->
    <section class="section-md bg-accent-gradient context-dark">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h4 class="heading-decorated">POURQUOI CHOISIR NOTRE ENTREPRISE ET PAS UNE AUTRE?</h4>
                </div>
            </div>
            <div class="row row-60">
                <!-- Owl Carousel-->
                <div class="owl-carousel owl-carousel-spacing-2" data-items="1" data-sm-items="2" data-xl-items="3" data-dots="true" data-nav="false" data-stage-padding="15" data-margin="30" data-mouse-drag="false" data-loop="true" data-autoplay="true">
                    <div class="item">
                        <!-- Post classic-->
                        <article class="post-classic post-minimal"><a class="post-minimal-image" href="image-post.html">
                                <img src="{{asset($content->whyimage1)}}" alt="" width="418" height="315" loading="lazy"/></a>
                          <div class="post-classic-title">
                                <h6><a href="#">{{$content->whytitle1}}</a></h6>
                            </div>
                        </article>
                    </div>
                    <div class="item">
                        <!-- Post classic-->
                        <article class="post-classic post-minimal"><a class="post-minimal-image" href="#">
                                <img src="{{asset($content->whyimage2)}}" alt="" width="418" height="315" loading="lazy"/></a>
                            <div class="post-classic-title">
                                <h6><a href="#">{{$content->whytitle2}}</a></h6>
                            </div>
                        </article>
                    </div>
                    <div class="item">
                        <!-- Post classic-->
                        <article class="post-classic post-minimal"><a class="post-minimal-image" href="gallery-post.html">
                                <img src="{{asset($content->whyimage3)}}" alt="" width="418" height="315" loading="lazy"/></a>
                            <div class="post-classic-title">
                                <h6><a href="#">{{$content->whytitle3}}</a></h6>
                            </div>
                        </article>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <!-- pricing table-->
    <section class="section-lg bg-gray-lighter text-center" id="formulas">
        <div class="bg-decor" data-parallax-scroll="{&quot;x&quot;: 80, &quot;y&quot;: -80,  &quot;smoothness&quot;: 30}">
            <img src="{{asset('front/images/bg-decor-3.png')}}" alt="" loading="lazy"/>
        </div>
        <div class="bg-decor d-flex align-items-center" data-parallax-scroll="{&quot;x&quot;: 80,  &quot;smoothness&quot;: 30}">
            <img src="{{asset('front/images/bg-decor-1.png')}}" alt="" loading="lazy"/>
        </div>
        <div class="container">
            <div class="row row-50 justify-content-lg-center">
                <div class="col-md-6 col-lg-4 col-xl-3">
                    <div class="pricing-table-wrap">
                        <div class="pricing-table-custom">
                            <h4>NOS FORMULES</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 col-xl-3">
                    <div class="pricing-table-wrap">
                        <div class="pricing-table pricing-table-label">
                            <div class="pricing-header">
                                <img src="{{asset('front/c1.png')}}" style="width: 100%" alt="">
                                <h5>{{$prices->title1}}</h5>
                          </div>
                            <div class="pricing-body">
                                <p>
                                    @foreach(json_decode($prices->d1) as $item)
                                       {{$item}} <br>
                                    @endforeach
                                </p>
                                <a href="{{route('service.details', ['title' => $prices->title1])}}"> <button class="btn btn-primary">PLUS DINFOS</button></a>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 col-xl-3">
                    <div class="pricing-table-wrap">
                        <div class="pricing-table">
                            <div class="pricing-header">
                                <img src="{{asset('front/c2.png')}}" style="width: 100%" alt="">
                                <h5>{{$prices->title2}}</h5>
                              </div>
                            <div class="pricing-body" style="padding-top: 10px">
                               <p>
                                   @foreach(json_decode($prices->d2) as $item)
                                       {{$item}} <br>
                                   @endforeach
                               </p>
                                <a href="{{route('service.details', ['title' => $prices->title2])}}"> <button class="btn btn-primary">PLUS DINFOS</button></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 col-xl-3">
                    <div class="pricing-table-wrap">
                        <div class="pricing-table">
                            <div class="pricing-header">
                                <img src="{{asset('front/c3.png')}}" style="width: 100%" alt="">
                                <h5>{{$prices->title3}}</h5>
                            </div>
                            <div class="pricing-body">
                                <p>
                                    @foreach(json_decode($prices->d3) as $item)
                                        {{$item}} <br>
                                    @endforeach
                                </p>
                                <a href="{{route('service.details', ['title' => $prices->title3])}}"> <button class="btn btn-primary">PLUS DINFOS</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section parallax-container context-dark decor-text" id="testimony" data-parallax-img="{{asset('front/images/bg-image-6.jpg')}}" data-content="Testimonials">
        <div class="parallax-content">
            <div class="container section-lg text-center">
                <h4 class="heading-decorated">CE QUE DISENT NOS CLIENTS</h4>
                <!-- Owl Carousel-->
                <div class="owl-carousel" data-items="1" data-stage-padding="15" data-loop="true" data-margin="30" data-dots="true" data-nav="false" data-autoplay="true">
                   @foreach($testimonial as $testimo)
                    <div class="item">
                        <!-- Quote default-->
                        <div class="quote-default">
                            <div class="quote-default__text">
                                <p class="q">{{$testimo->review}}</p>
                            </div>
                            <p class="quote-default__cite">{{$testimo->name}}</p>
                            <p class="q">{{$testimo->designation}}</p>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

    <!-- counters-->
    <section class="section parallax-container context-dark" data-parallax-img="{{asset('front/images/parallax-1.jpg')}}">
        <div class="parallax-content">
            <div class="container section-md">
                <div class="row justify-content-md-center row-50">
                    <div class="col-md-6 col-lg-3">
                        <!-- Box counter-->
                        <article class="box-counter">
                            <div class="box-counter__icon linear-icon-coffee-cup"></div>
                            <div class="box-counter-main">
                                <div class="counter">{{$content->projects}}</div>
                            </div>
                            <p class="box-counter-title">Projets annuellement</p>
                        </article>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <!-- Box counter-->
                        <article class="box-counter">
                            <div class="box-counter__icon linear-icon-cube"></div>
                            <div class="box-counter-main">
                                <div class="counter">{{$content->awards}}</div>
                            </div>
                            <p class="box-counter-title">Récompenses</p>
                        </article>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <!-- Box counter-->
                        <article class="box-counter">
                            <div class="box-counter__icon linear-icon-chevrons-expand-horizontal"></div>
                            <div class="box-counter-main">
                                <div class="counter">{{$content->reviews}}</div><span>%</span>
                            </div>
                            <p class="box-counter-title">Avis positifs</p>
                        </article>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <!-- Box counter-->
                        <article class="box-counter">
                            <div class="box-counter__icon linear-icon-mustache-glasses"></div>
                            <div class="box-counter-main">
                                <div class="counter">{{$content->customers}}</div><span>k</span>
                            </div>
                            <p class="box-counter-title">Clients heureux</p>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
