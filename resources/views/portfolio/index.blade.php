@extends('layouts.admin')

@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Domicile</a></li>
                            <li class="breadcrumb-item active">Projets</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- SELECT2 EXAMPLE -->
                <div class="card card-default">
                    <div class="card-header">
                        <h3 class="card-title">
                            Projets
                        </h3>

                        <div class="card-tools">
                            <a href="" data-toggle="modal" data-target="#employeeImport"
                               class="btn btn-sm btn-primary btn-round" title="">Ajouter un nouveau Projet</a>
                        </div>
                    </div>
                    @csrf
                    <!-- /.card-header -->
                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        @if ($errors->any())
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger">
                                    <p>{{$error}}</p>
                                </div>
                            @endforeach
                        @endif
                        <table
                            class="table table-bordered table-hover table-striped js-basic-example dataTable table-custom">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nom</th>
                                <th>Image</th>
                                <th>Category</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($portfolios as $portfolio)
                                <tr>
                                    <td>{{$portfolio->id}}</td>
                                    <td>{{$portfolio->name}}</td>
                                    <td><img style="height: 40px" src="{{asset($portfolio->image)}}" alt=""></td>
                                    <td>{{$portfolio->e2}}</td>
                                    <td>
                                        <a style="color: white" data-toggle="modal"
                                           data-target="#editmodal{{$portfolio->id}}"
                                           class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>
                                        <div class="modal fade" id="editmodal{{$portfolio->id}}" tabindex="-1"
                                             role="dialog" aria-labelledby="employeeImportTitle"
                                             aria-hidden="true">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <form method="post"
                                                          action="{{route('portfolio.update', $portfolio->id)}}"
                                                          enctype="multipart/form-data">
                                                        @csrf
                                                        @method('put')
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalCenterTitle">
                                                                Ajouter un nouveau Témoignage</h5>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <label>Nom</label>
                                                                    <div class="input-group mb-3">
                                                                        <input value="{{$portfolio->name}}" name="name"
                                                                               required class="form-control"
                                                                               type="text">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Category</label>
                                                                    <div class="input-group mb-3">
                                                                        <select class="form-control" name="category"
                                                                                id="">
                                                                            <option
                                                                                {{$portfolio->e2 == "SITE WEB" ? 'selected':''}} value="SITE WEB">
                                                                                SITE WEB
                                                                            </option>
                                                                            <option
                                                                                {{$portfolio->e2 == "APPLICATION MOBILE" ? 'selected':''}} value="APPLICATION MOBILE">
                                                                                APPLICATION MOBILE
                                                                            </option>
                                                                            <option
                                                                                {{$portfolio->e2 == "BUILDER" ? 'selected':''}} value="BUILDER">
                                                                                BUILDER
                                                                            </option>
                                                                            <option
                                                                                {{$portfolio->e2 == "CRM" ? 'selected':''}} value="CRM">
                                                                                CRM
                                                                            </option>
                                                                            <option
                                                                                {{$portfolio->e2 == "VIDÉO 2D" ? 'selected':''}} value="VIDÉO 2D">
                                                                                VIDÉO 2D
                                                                            </option>
                                                                            <option
                                                                                {{$portfolio->e2 == "JEUX" ? 'selected':''}} value="JEUX">
                                                                                JEUX
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Image 1277 x 1376 </label>
                                                                    <div class="input-group mb-3">
                                                                        <input name="image" class="form-control"
                                                                               type="file">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>FlipBook </label>
                                                                    <div class="input-group mb-3">
                                                                        <input name="e1" value="{{$portfolio->e1}}"
                                                                               required class="form-control" type="url">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Video Url </label>
                                                                    <div class="input-group mb-3">
                                                                        <input name="video" value="{{$portfolio->e3}}"
                                                                               required class="form-control"
                                                                               type="text">
                                                                    </div>
                                                                </div>
                                                                {{--                                                                                                                                <div class="col-md-6">--}}
                                                                {{--                                                                                                                                    <label>Technologie 3 </label>--}}
                                                                {{--                                                                                                                                    <div class="input-group mb-3">--}}
                                                                {{--                                                                                                                                        <input name="e3" value="{{$portfolio->e3 }}" required class="form-control" type="text">--}}
                                                                {{--                                                                                                                                    </div>--}}
                                                                {{--                                                                                                                                </div>--}}
                                                                {{--                                                                                                                                <div class="col-md-6">--}}
                                                                {{--                                                                                                                                    <label>Technologie 4 </label>--}}
                                                                {{--                                                                                                                                    <div class="input-group mb-3">--}}
                                                                {{--                                                                                                                                        <input name="e4" value="{{$portfolio->e4}}" required class="form-control" type="text">--}}
                                                                {{--                                                                                                                                    </div>--}}
                                                                {{--                                                                                                                                </div>--}}
                                                                <div class="col-md-6">
                                                                    <label>Lien de site Web </label>
                                                                    <div class="input-group mb-3">
                                                                        <input name="website"
                                                                               value="{{$portfolio->website}}"
                                                                               class="form-control" type="url">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Lien vers l'application Play Store </label>
                                                                    <div class="input-group mb-3">
                                                                        <input name="android"
                                                                               value="{{$portfolio->android}}"
                                                                               class="form-control" type="url">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Lien vers l'application Ios Store </label>
                                                                    <div class="input-group mb-3">
                                                                        <input name="ios" value="{{$portfolio->ios}}"
                                                                               class="form-control" type="url">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <label>La description</label>
                                                                    <div class="input-group mb-3">
                                    <textarea name="description" class="form-control summernote" cols="30"
                                              rows="10">{{$portfolio->description}}</textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger"
                                                                    data-dismiss="modal">Fermer
                                                            </button>
                                                            <button type="submit" class="btn btn-primary"
                                                                    onClick="this.form.submit(); this.disabled=true; this.value='Sending…'; ">
                                                                Sauver
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <form style="display: inline-block"
                                              action="{{route('portfolio.destroy', $portfolio->id)}}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" href=""
                                                    onclick=" return confirm('Are you you want to remove this portfolio?')"
                                                    class="btn btn-sm btn-danger"><i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="modal fade" id="employeeImport" tabindex="-1" role="dialog" aria-labelledby="employeeImportTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form method="post" action="{{route('portfolio.store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">Ajouter un nouveau Témoignage</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Nom</label>
                                <div class="input-group mb-3">
                                    <input name="name" required class="form-control" type="text">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>Category</label>
                                <div class="input-group mb-3">
                                    <select class="form-control" name="category" id="">
                                        <option value="SITE WEB">SITE WEB</option>
                                        <option value="APPLICATION MOBILE">APPLICATION MOBILE</option>
                                        <option value="BUILDER">BUILDER</option>
                                        <option value="JEUX">JEUX</option>
                                        <option value="CRM">CRM</option>
                                        <option value="VIDÉO 2D">VIDÉO 2D</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>Image 1277 x 1376 </label>
                                <div class="input-group mb-3">
                                    <input name="image" required class="form-control" type="file">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>FlipBook </label>
                                <div class="input-group mb-3">
                                    <input name="e1" required class="form-control" type="url">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>Video Url </label>
                                <div class="input-group mb-3">
                                    <input name="video" required class="form-control" type="text">
                                </div>
                            </div>
                            <!--        <div class="col-md-6">
                                   <label>Technologie 3 </label>
                                   <div class="input-group mb-3">
                                       <input name="e3" required class="form-control" type="text">
                                   </div>
                               </div>
                               <div class="col-md-6">
                                   <label>Technologie 4 </label>
                                   <div class="input-group mb-3">
                                       <input name="e4" required class="form-control" type="text">
                                   </div>
                               </div>-->
                            <div class="col-md-6">
                                <label>Lien de site Web </label>
                                <div class="input-group mb-3">
                                    <input name="website" class="form-control" type="url">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>Lien vers l'application Play Store </label>
                                <div class="input-group mb-3">
                                    <input name="android" class="form-control" type="url">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>Lien vers l'application Ios Store </label>
                                <div class="input-group mb-3">
                                    <input name="ios" class="form-control" type="url">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label>La description</label>
                                <div class="input-group mb-3">
                                    <textarea name="description" class="form-control summernote" cols="30"
                                              rows="10"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Fermer</button>
                        <button type="submit" class="btn btn-primary"
                                onClick="this.form.submit(); this.disabled=true; this.value='Sending…'; ">Sauver
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
